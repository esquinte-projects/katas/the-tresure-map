import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { BuilderModule, WorldBuilderService } from './modules/builder';
import { PlayModule, PlayService } from './modules/play';
import { FileReaderService, ReaderModule } from './modules/reader';
import { WriterModule, WriterService } from './modules/writer';

(async function bootstrap() {
  const app = await NestFactory.createApplicationContext(AppModule);

  // extract lines
  const fileReaderService = app.select(ReaderModule).get(FileReaderService, { strict: true });
  const lines = await fileReaderService.parse(process.env.INPUT_FILE);

  // build world
  const worldBuilderService = app.select(BuilderModule).get(WorldBuilderService, { strict: true });
  const world = worldBuilderService.build(lines);

  // play movements
  const playService = app.select(PlayModule).get(PlayService, { strict: true });
  playService.start(world);

  // output file
  const writer = app.select(WriterModule).get(WriterService, { strict: true });
  writer.write(world);
})();
