import { Module } from '@nestjs/common';
import { PlayService } from './services/play.service';

@Module({
  providers: [PlayService],
})
export class PlayModule {}
