import { Adventurer } from '@models/adventurer.model';
import { Cell } from '@models/cell.model';
import { Direction, Movement } from '@models/enums/business';
import { Map } from '@models/map.model';
import { Mountain } from '@models/mountain.model';
import { Tresure } from '@models/tresure.model';
import { World } from '@models/world.model';
import { Test } from '@nestjs/testing';
import { LoggerModule, LoggerService } from '@shared/logger';
import { PlayService } from './play.service';

describe('PlayService', () => {
  let service: PlayService;
  let logger: LoggerService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({ providers: [PlayService], imports: [LoggerModule] }).compile();

    service = moduleRef.get<PlayService>(PlayService);
    logger = moduleRef.get<LoggerService>(LoggerService);
  });

  describe('start', () => {
    describe('move adventurer', () => {
      describe('forward', () => {
        it('should move adventurer to south', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.SOUTH, [Movement.Enum.FORWARD]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(2);
          expect(world.adventurers[0].y).toBe(3);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.SOUTH);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBeNull();
          expect(world.map.board[2][3].adventurer).toBe(adventurer);
        });

        it('should move adventurer to north', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.NORTH, [Movement.Enum.FORWARD]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(2);
          expect(world.adventurers[0].y).toBe(1);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.NORTH);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBeNull();
          expect(world.map.board[2][1].adventurer).toBe(adventurer);
        });

        it('should move adventurer to east', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.EAST, [Movement.Enum.FORWARD]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(3);
          expect(world.adventurers[0].y).toBe(2);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.EAST);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBeNull();
          expect(world.map.board[3][2].adventurer).toBe(adventurer);
        });

        it('should move adventurer to west', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.WEST, [Movement.Enum.FORWARD]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(1);
          expect(world.adventurers[0].y).toBe(2);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.WEST);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBeNull();
          expect(world.map.board[1][2].adventurer).toBe(adventurer);
        });
      });

      describe('left', () => {
        it('should turn left adventurer with south direction', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.SOUTH, [Movement.Enum.LEFT]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(2);
          expect(world.adventurers[0].y).toBe(2);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.EAST);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBe(adventurer);
        });

        it('should turn left adventurer with north direction', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.NORTH, [Movement.Enum.LEFT]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(2);
          expect(world.adventurers[0].y).toBe(2);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.WEST);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBe(adventurer);
        });

        it('should turn left adventurer with east direction', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.EAST, [Movement.Enum.LEFT]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(2);
          expect(world.adventurers[0].y).toBe(2);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.NORTH);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBe(adventurer);
        });

        it('should turn left adventurer with west direction', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.WEST, [Movement.Enum.LEFT]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(2);
          expect(world.adventurers[0].y).toBe(2);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.SOUTH);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBe(adventurer);
        });
      });

      describe('right', () => {
        it('should turn right adventurer with south direction', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.SOUTH, [Movement.Enum.RIGHT]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(2);
          expect(world.adventurers[0].y).toBe(2);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.WEST);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBe(adventurer);
        });

        it('should turn right adventurer with north direction', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.NORTH, [Movement.Enum.RIGHT]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(2);
          expect(world.adventurers[0].y).toBe(2);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.EAST);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBe(adventurer);
        });

        it('should turn right adventurer with east direction', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.EAST, [Movement.Enum.RIGHT]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(2);
          expect(world.adventurers[0].y).toBe(2);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.SOUTH);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBe(adventurer);
        });

        it('should turn right adventurer with west direction', () => {
          const world = new World();
          world.map = new Map(5, 5);
          world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

          const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.WEST, [Movement.Enum.RIGHT]);
          world.adventurers.push(adventurer);

          world.map.board[2][2].adventurer = adventurer;

          service.start(world);

          expect(world.adventurers[0].x).toBe(2);
          expect(world.adventurers[0].y).toBe(2);
          expect(world.adventurers[0].direction).toBe(Direction.Enum.NORTH);
          expect(world.adventurers[0].movements).toHaveLength(0);

          expect(world.map.board[2][2].adventurer).toBe(adventurer);
        });
      });
    });

    describe('pick-up tresure', () => {
      it('should move adventurer and pick-up tresure', () => {
        const world = new World();
        world.map = new Map(5, 5);
        world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

        const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.SOUTH, [Movement.Enum.FORWARD]);
        world.adventurers.push(adventurer);

        const tresure = new Tresure(2, 3, 1);
        world.tresures.push(tresure);

        world.map.board[2][2].adventurer = adventurer;
        world.map.board[2][3].resource = tresure;

        service.start(world);

        expect(world.adventurers[0].x).toBe(2);
        expect(world.adventurers[0].y).toBe(3);
        expect(world.adventurers[0].nbTresures).toBe(1);

        expect((world.tresures[0] as Tresure).nb).toBe(0);
      });

      it('should move adventurer and not pick-up tresure', () => {
        const world = new World();
        world.map = new Map(5, 5);
        world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

        const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.SOUTH, [Movement.Enum.FORWARD]);
        world.adventurers.push(adventurer);

        const tresure = new Tresure(2, 3, 0);
        world.tresures.push(tresure);

        world.map.board[2][2].adventurer = adventurer;
        world.map.board[2][3].resource = tresure;

        service.start(world);

        expect(world.adventurers[0].x).toBe(2);
        expect(world.adventurers[0].y).toBe(3);
        expect(world.adventurers[0].nbTresures).toBe(0);

        expect((world.tresures[0] as Tresure).nb).toBe(0);
      });
    });

    it('should not move adventurer when next move is outside', () => {
      const mockLogOutsideNextCoord = jest.spyOn(logger, 'logOutsideNextCoord').mockImplementation();

      const world = new World();
      world.map = new Map(5, 5);
      world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

      const adventurer = new Adventurer('Lara', 4, 4, Direction.Enum.SOUTH, [Movement.Enum.FORWARD]);
      world.adventurers.push(adventurer);

      world.map.board[4][4].adventurer = adventurer;

      service.start(world);

      expect(world.adventurers[0].x).toBe(4);
      expect(world.adventurers[0].y).toBe(4);

      expect(mockLogOutsideNextCoord).toHaveBeenCalledTimes(1);
      expect(mockLogOutsideNextCoord).toHaveBeenCalledWith({ coordX: 4, coordY: 4 }, { coordX: 4, coordY: 5 }, 'Lara', 'PlayService');
    });

    it('should not move adventurer when next move is a mountain', () => {
      const mockLogMountainNextCoord = jest.spyOn(logger, 'logMountainNextCoord').mockImplementation();

      const world = new World();
      world.map = new Map(5, 5);
      world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

      const adventurer = new Adventurer('Lara', 3, 3, Direction.Enum.SOUTH, [Movement.Enum.FORWARD]);
      world.adventurers.push(adventurer);

      const mountain = new Mountain(3, 4);
      world.mountains.push(mountain);

      world.map.board[3][3].adventurer = adventurer;
      world.map.board[3][4].resource = mountain;

      service.start(world);

      expect(world.adventurers[0].x).toBe(3);
      expect(world.adventurers[0].y).toBe(3);

      expect(mockLogMountainNextCoord).toHaveBeenCalledTimes(1);
      expect(mockLogMountainNextCoord).toHaveBeenCalledWith({ coordX: 3, coordY: 3 }, { coordX: 3, coordY: 4 }, 'Lara', 'PlayService');
    });

    it('should not move adventurer when other adventurer already present', () => {
      const mockLogAlreadyAdventurerNextCoord = jest.spyOn(logger, 'logAlreadyAdventurerNextCoord').mockImplementation();

      const world = new World();
      world.map = new Map(5, 5);
      world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

      const adventurer1 = new Adventurer('Lara', 2, 2, Direction.Enum.SOUTH, [Movement.Enum.FORWARD]);
      const adventurer2 = new Adventurer('John', 2, 3, Direction.Enum.SOUTH, [Movement.Enum.FORWARD]);
      world.adventurers.push(adventurer1, adventurer2);

      world.map.board[2][2].adventurer = adventurer1;
      world.map.board[2][3].adventurer = adventurer2;

      service.start(world);

      expect(world.adventurers[0].x).toBe(2);
      expect(world.adventurers[0].y).toBe(2);

      expect(world.adventurers[1].x).toBe(2);
      expect(world.adventurers[1].y).toBe(4);

      expect(mockLogAlreadyAdventurerNextCoord).toHaveBeenCalledTimes(1);
      expect(mockLogAlreadyAdventurerNextCoord).toHaveBeenCalledWith({ coordX: 2, coordY: 2 }, { coordX: 2, coordY: 3 }, 'Lara', 'PlayService');
    });
  });
});
