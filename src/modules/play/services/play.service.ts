import { Adventurer, Map, Move, Tresure, World } from '@models';
import { Direction, Movement, ResourceType } from '@models/enums/business';
import { Injectable } from '@nestjs/common';
import { LoggerService } from '@shared/logger';

/**
 * Service de gestion des mouvements des aventuriers
 */
@Injectable()
export class PlayService {
  private map: Map;
  private currentAdventurer: Adventurer;
  private currentMove: Move;

  constructor(private loggerService: LoggerService) {}

  /**
   * Lance la partie
   * @param world le monde
   */
  start(world: World) {
    this.loggerService.logInfo('Playing movements...', PlayService.name);

    const start = process.hrtime.bigint();

    this.map = world.map;
    this.moveAdenturersRoundByRound(world.adventurers);

    const end = process.hrtime.bigint();

    this.loggerService.logInfo(`Movements played in ${(end - start) / BigInt(1e3)} microseconds`, PlayService.name);
  }

  /**
   * Avance les aventuriers tour par tour
   * @param adventurers
   */
  private moveAdenturersRoundByRound(adventurers: Adventurer[]) {
    const playableAdventurers = adventurers.filter((adventurer) => adventurer.movements.length > 0);

    if (playableAdventurers.length > 0) {
      adventurers.forEach((adventurer) => {
        this.moveAdventurer(adventurer);
      });
      this.moveAdenturersRoundByRound(playableAdventurers);
    }
  }

  /**
   * Avance un aventurier
   * @param adventurer l'aventurier
   */
  private moveAdventurer(adventurer: Adventurer) {
    this.currentAdventurer = adventurer;

    this.currentMove = new Move(this.currentAdventurer.x, this.currentAdventurer.y, this.currentAdventurer.direction, this.currentAdventurer.movements.shift());

    this.computeNextMove();

    if (this.isNextCoordValid()) {
      if (this.currentMove.state === Movement.Enum.FORWARD) {
        const currentCell = this.map.board[this.currentMove.currentX][this.currentMove.currentY];
        currentCell.adventurer = null;

        const nextCell = this.map.board[this.currentMove.nextX][this.currentMove.nextY];
        nextCell.adventurer = this.currentAdventurer;

        if (nextCell.resource?.type === ResourceType.Enum.TRESURE) {
          const tresure = nextCell.resource as Tresure;
          if (tresure.nb > 0) {
            tresure.nb--;
            this.currentAdventurer.nbTresures++;
          }
        }
        this.currentAdventurer.x = this.currentMove.nextX;
        this.currentAdventurer.y = this.currentMove.nextY;
      }
      this.currentAdventurer.direction = this.currentMove.nextDirection;
    }
  }

  /**
   * Calcule le prochain mouvement
   */
  private computeNextMove() {
    const movement = this.currentMove.state;

    if (movement === Movement.Enum.FORWARD) {
      this.computeNextForwardMove();
    }

    if (movement === Movement.Enum.LEFT) {
      this.computeNextLeftMove();
    }

    if (movement === Movement.Enum.RIGHT) {
      this.computeNextRightMove();
    }
  }

  /**
   * Calcule le prochain mouvement "avancer"
   */
  private computeNextForwardMove() {
    const direction = this.currentMove.direction;

    if (direction === Direction.Enum.EAST) {
      this.currentMove.nextX++;
    }

    if (direction === Direction.Enum.NORTH) {
      this.currentMove.nextY--;
    }

    if (direction === Direction.Enum.SOUTH) {
      this.currentMove.nextY++;
    }

    if (direction === Direction.Enum.WEST) {
      this.currentMove.nextX--;
    }
  }

  /**
   * Calcule le prochain mouvement "tourner à gauche"
   */
  private computeNextLeftMove() {
    const direction = this.currentMove.direction;

    if (direction === Direction.Enum.EAST) {
      this.currentMove.nextDirection = Direction.Enum.NORTH;
    }

    if (direction === Direction.Enum.NORTH) {
      this.currentMove.nextDirection = Direction.Enum.WEST;
    }

    if (direction === Direction.Enum.SOUTH) {
      this.currentMove.nextDirection = Direction.Enum.EAST;
    }

    if (direction === Direction.Enum.WEST) {
      this.currentMove.nextDirection = Direction.Enum.SOUTH;
    }
  }

  /**
   * Calcule le prochain mouvement "tourner à droites"
   */
  private computeNextRightMove() {
    const direction = this.currentMove.direction;

    if (direction === Direction.Enum.EAST) {
      this.currentMove.nextDirection = Direction.Enum.SOUTH;
    }

    if (direction === Direction.Enum.NORTH) {
      this.currentMove.nextDirection = Direction.Enum.EAST;
    }

    if (direction === Direction.Enum.SOUTH) {
      this.currentMove.nextDirection = Direction.Enum.WEST;
    }

    if (direction === Direction.Enum.WEST) {
      this.currentMove.nextDirection = Direction.Enum.NORTH;
    }
  }

  /**
   * Détermine si les coordonnées du prochain mouvement sont valides
   * @returns true si les coordonnées sont validées sinon false
   */
  private isNextCoordValid(): boolean {
    const currentX = this.currentMove.currentX;
    const currentY = this.currentMove.currentY;

    const nextX = this.currentMove.nextX;
    const nextY = this.currentMove.nextY;

    if (currentX === nextX && currentY === nextY) {
      return true;
    }

    const isNotOutside = nextX >= 0 && this.map.width - 1 >= nextX && nextY >= 0 && this.map.height - 1 >= nextY;
    if (!isNotOutside) {
      this.loggerService.logOutsideNextCoord({ coordX: currentX, coordY: currentY }, { coordX: nextX, coordY: nextY }, this.currentAdventurer.name, PlayService.name);
      return false;
    }

    const isNoMountain = this.map.board[nextX][nextY].resource?.type !== ResourceType.Enum.MOUNTAIN;
    if (!isNoMountain) {
      this.loggerService.logMountainNextCoord({ coordX: currentX, coordY: currentY }, { coordX: nextX, coordY: nextY }, this.currentAdventurer.name, PlayService.name);
      return false;
    }

    const isAlreadyAdventurer = this.map.board[nextX][nextY].adventurer;
    if (isAlreadyAdventurer) {
      this.loggerService.logAlreadyAdventurerNextCoord({ coordX: currentX, coordY: currentY }, { coordX: nextX, coordY: nextY }, this.currentAdventurer.name, PlayService.name);
      return false;
    }

    return true;
  }
}
