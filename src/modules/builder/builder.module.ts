import { Module } from '@nestjs/common';
import { MapBuilderService } from './services/map/map-builder.service';
import { WorldBuilderService } from './services/world/world-builder.service';

@Module({
  providers: [WorldBuilderService, MapBuilderService],
})
export class BuilderModule {}
