import { Adventurer, Cell, Map } from '@models';
import { ResourceType } from '@models/enums/business';
import { Resource } from '@models/interfaces';
import { Injectable } from '@nestjs/common';

/**
 * Service de construction de la carte
 */
@Injectable()
export class MapBuilderService {
  /**
   * Alimente les ressources dans la carte
   * @param map la carte
   * @param resources la liste des ressources
   */
  populateResources(map: Map, resources: Resource[]) {
    map.board = [];

    for (let x = 0; x < map.width; x++) {
      map.board[x] = [];
      for (let y = 0; y < map.height; y++) {
        const cell = new Cell();
        const resourcesFiltered = resources.filter((resource) => resource.x === x && resource.y === y);
        if (resourcesFiltered.length > 0) {
          cell.resource = this.findResource(resourcesFiltered);
        }
        map.board[x][y] = cell;
      }
    }
  }

  /**
   * Ajoute les aventuriers dans la carte.
   * Si une montagne ou un autre aventurier se trouve déjà sur les coordonnées, l'aventurier actuel est ignoré
   * @param map la carte
   * @param adventurers la liste des aventuriers
   */
  addAdventurers(map: Map, adventurers: Adventurer[]) {
    for (let i = 0; i < adventurers.length; i++) {
      const adventurer = adventurers[i];
      const cell = map.board[adventurer.x][adventurer.y];

      if (cell.resource?.type === ResourceType.Enum.MOUNTAIN || cell.adventurer != null) {
        adventurers.splice(i--, 1);
      } else {
        cell.adventurer = adventurer;
      }
    }
  }

  /**
   * Récupère la ressource.
   * Si un trésor et une montagne ressources sont trouvées, on renvoie une montagne
   * @param resources les ressources
   * @returns la ressource récupérée
   */
  private findResource(resources: Resource[]): Resource {
    if (resources.length === 1) {
      return resources[0];
    }

    return resources.find((resource) => resource.type === ResourceType.Enum.MOUNTAIN);
  }
}
