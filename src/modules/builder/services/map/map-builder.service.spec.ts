import { Adventurer } from '@models/adventurer.model';
import { Cell } from '@models/cell.model';
import { Direction, Movement } from '@models/enums/business';
import { Map } from '@models/map.model';
import { Mountain } from '@models/mountain.model';
import { Tresure } from '@models/tresure.model';
import { Test } from '@nestjs/testing';
import { MapBuilderService } from '../map/map-builder.service';

describe('MapBuilderService', () => {
  let service: MapBuilderService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [MapBuilderService],
      imports: [],
    }).compile();

    service = moduleRef.get<MapBuilderService>(MapBuilderService);
  });

  describe('populateResources', () => {
    it('should populate resources into map', () => {
      const map = new Map(5, 5);
      const mountain = new Mountain(1, 2);
      const tresure = new Tresure(3, 4, 2);

      service.populateResources(map, [mountain, tresure]);

      expect(map.board[1][2].resource).toBe(mountain);
      expect(map.board[3][4].resource).toBe(tresure);
    });

    it('should populate mountain over tresure when same coordinate', () => {
      const map = new Map(5, 5);
      const mountain = new Mountain(1, 2);
      const tresure = new Tresure(1, 2, 1);

      service.populateResources(map, [mountain, tresure]);

      expect(map.board[1][2].resource).toBe(mountain);
    });
  });

  describe('addAdventurers', () => {
    it('should add adventurers into map', () => {
      const map = new Map(5, 5);
      map.board = Array.from({ length: map.width }, () => Array.from({ length: map.height }, () => new Cell()));

      const adventurer1 = new Adventurer('Lara', 2, 2, Direction.Enum.NORTH, [Movement.Enum.FORWARD, Movement.Enum.LEFT]);
      const adventurer2 = new Adventurer('John', 4, 1, Direction.Enum.WEST, [Movement.Enum.RIGHT, Movement.Enum.FORWARD]);

      service.addAdventurers(map, [adventurer1, adventurer2]);

      expect(map.board[2][2].adventurer).toBe(adventurer1);
      expect(map.board[4][1].adventurer).toBe(adventurer2);
    });

    it('should have only one adventurer in one cell', () => {
      const map = new Map(5, 5);
      map.board = Array.from({ length: map.width }, () => Array.from({ length: map.height }, () => new Cell()));

      const adventurer1 = new Adventurer('Lara', 2, 2, Direction.Enum.NORTH, [Movement.Enum.FORWARD, Movement.Enum.LEFT]);
      const adventurer2 = new Adventurer('John', 2, 2, Direction.Enum.WEST, [Movement.Enum.RIGHT, Movement.Enum.FORWARD]);

      service.addAdventurers(map, [adventurer1, adventurer2]);

      expect(map.board[2][2].adventurer).toBe(adventurer1);
    });

    it('should not add adventurer if mountain already present', () => {
      const map = new Map(5, 5);
      map.board = Array.from({ length: map.width }, () => Array.from({ length: map.height }, () => new Cell()));

      const mountain = new Mountain(2, 2);

      map.board[2][2].resource = mountain;

      const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.NORTH, [Movement.Enum.FORWARD, Movement.Enum.LEFT]);

      service.addAdventurers(map, [adventurer]);

      expect(map.board[2][2].adventurer).toBeUndefined();
    });

    it('should add adventurer if tresure already present', () => {
      const map = new Map(5, 5);
      map.board = Array.from({ length: map.width }, () => Array.from({ length: map.height }, () => new Cell()));

      const tresure = new Tresure(2, 2, 1);

      map.board[2][2].resource = tresure;

      const adventurer = new Adventurer('Lara', 2, 2, Direction.Enum.NORTH, [Movement.Enum.FORWARD, Movement.Enum.LEFT]);

      service.addAdventurers(map, [adventurer]);

      expect(map.board[2][2].adventurer).toBe(adventurer);
    });
  });
});
