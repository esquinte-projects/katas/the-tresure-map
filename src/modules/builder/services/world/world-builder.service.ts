import { Adventurer, Map, Mountain, Tresure, World } from '@models';
import { Direction, Movement, ResourceType } from '@models/enums/business';
import { Injectable } from '@nestjs/common';
import { LoggerService } from '@shared/logger';
import { MapBuilderService } from '../map/map-builder.service';

/**
 * Service de construction du monde
 */
@Injectable()
export class WorldBuilderService {
  constructor(private mapBuilderService: MapBuilderService, private loggerService: LoggerService) {}

  /**
   * Construit le monde
   * @param lines les lignes de données
   * @returns le monde construit
   */
  build(lines: string[]): World {
    this.loggerService.logInfo('Building the world...', WorldBuilderService.name);

    const start = process.hrtime.bigint();

    const world = this.createWorld(lines);

    this.mapBuilderService.populateResources(world.map, [...world.mountains, ...world.tresures]);
    this.mapBuilderService.addAdventurers(world.map, world.adventurers);

    const end = process.hrtime.bigint();
    this.loggerService.logInfo(`World built in ${(end - start) / BigInt(1e3)} microseconds`, WorldBuilderService.name);

    return world;
  }

  /**
   * Crée le monde
   * @param lines les lignes de données
   * @returns le monde créé
   */
  private createWorld(lines: string[]): World {
    const world = new World();

    lines.forEach((line) => {
      const cols = line.split(process.env.INPUT_SEPARATOR);
      const resourceType = cols[0];

      if (resourceType === ResourceType.Enum.MAP) {
        world.map = new Map(+cols[1], +cols[2]);
      }

      if (resourceType === ResourceType.Enum.MOUNTAIN) {
        world.mountains.push(new Mountain(+cols[1], +cols[2]));
      }

      if (resourceType === ResourceType.Enum.TRESURE) {
        world.tresures.push(new Tresure(+cols[1], +cols[2], +cols[3]));
      }

      if (resourceType === ResourceType.Enum.ADVENTURER) {
        const direction = cols[4] as Direction.Enum;
        const movements = cols[5].split('').map((movement) => movement as Movement.Enum);
        world.adventurers.push(new Adventurer(cols[1], +cols[2], +cols[3], direction, movements));
      }
    });

    return world;
  }
}
