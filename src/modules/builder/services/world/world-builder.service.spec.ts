import { Cell } from '@models/cell.model';
import { Direction, Movement, ResourceType } from '@models/enums/business';
import { Tresure } from '@models/tresure.model';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { LoggerModule } from '@shared/logger';
import { MapBuilderService } from '../map/map-builder.service';
import { WorldBuilderService } from './world-builder.service';

describe('WorldBuilderService', () => {
  let service: WorldBuilderService;
  let mapBuilder: MapBuilderService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [WorldBuilderService, MapBuilderService],
      imports: [ConfigModule.forRoot({ envFilePath: 'test/config-test.env' }), LoggerModule],
    }).compile();

    service = moduleRef.get<WorldBuilderService>(WorldBuilderService);
    mapBuilder = moduleRef.get<MapBuilderService>(MapBuilderService);
  });

  describe('build', () => {
    it('should return world built', () => {
      const mockPopulateResources = jest.spyOn(mapBuilder, 'populateResources').mockImplementation((map) => {
        map.board = Array.from({ length: map.width }, () => Array.from({ length: map.height }, () => new Cell()));
      });
      const mockAddAdventurers = jest.spyOn(mapBuilder, 'addAdventurers').mockImplementation();

      const lines = ['C - 5 - 6', 'M - 1 - 2', 'T - 2 - 3 - 1', 'A - Lara - 3 - 4 - S - AGD'];
      const world = service.build(lines);

      expect(world).toBeDefined();

      expect(world.map).toBeDefined();
      expect(world.map.width).toBe(5);
      expect(world.map.height).toBe(6);
      expect(world.map.board).toHaveLength(5);
      expect(world.map.board.every((column) => column.length === 6)).toBe(true);

      expect(world.mountains).toBeDefined();
      expect(world.mountains).toHaveLength(1);
      expect(world.mountains[0].type).toBe(ResourceType.Enum.MOUNTAIN);
      expect(world.mountains[0].x).toBe(1);
      expect(world.mountains[0].y).toBe(2);

      expect(world.tresures).toBeDefined();
      expect(world.tresures).toHaveLength(1);
      expect(world.tresures[0].type).toBe(ResourceType.Enum.TRESURE);
      expect(world.tresures[0].x).toBe(2);
      expect(world.tresures[0].y).toBe(3);
      expect((world.tresures[0] as Tresure).nb).toBe(1);

      expect(world.adventurers).toBeDefined();
      expect(world.adventurers).toHaveLength(1);
      expect(world.adventurers[0].type).toBe(ResourceType.Enum.ADVENTURER);
      expect(world.adventurers[0].name).toBe('Lara');
      expect(world.adventurers[0].x).toBe(3);
      expect(world.adventurers[0].y).toBe(4);
      expect(world.adventurers[0].direction).toBe(Direction.Enum.SOUTH);
      expect(world.adventurers[0].movements).toEqual([Movement.Enum.FORWARD, Movement.Enum.LEFT, Movement.Enum.RIGHT]);

      expect(mockPopulateResources).toHaveBeenCalled();
      expect(mockPopulateResources).toHaveBeenCalledWith(world.map, [...world.mountains, ...world.tresures]);
      expect(mockAddAdventurers).toHaveBeenCalled();
      expect(mockAddAdventurers).toHaveBeenCalledWith(world.map, world.adventurers);
    });
  });
});
