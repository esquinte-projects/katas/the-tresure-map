import { Adventurer, Mountain, Tresure, World } from '@models';
import { ResourceType } from '@models/enums/business';
import { Resource } from '@models/interfaces';
import { Injectable } from '@nestjs/common';
import { LoggerService } from '@shared/logger';
import { createWriteStream, WriteStream } from 'fs';

/**
 * Service d'écriture d'un fichier
 */
@Injectable()
export class WriterService {
  private output: WriteStream;
  private spacing: number;
  private readonly gapSize: number = +process.env.OUTPUT_GRAPH_GAP_SIZE;
  private lastElement: boolean;

  constructor(private loggerService: LoggerService) {}

  /**
   * Génère les fichiers de sortie
   * @param world le monde
   */
  write(world: World) {
    this.loggerService.logInfo('Writing output file...', WriterService.name);

    const start = process.hrtime.bigint();

    this.writeFile(world);
    this.writeGraph(world);

    const end = process.hrtime.bigint();

    this.loggerService.logInfo(`Files "${process.env.OUTPUT_FILE}" and "${process.env.OUTPUT_GRAPH}" generated in ${(end - start) / BigInt(1e3)} microseconds`, WriterService.name);
  }

  /**
   * Génère le fichier sous forme de liste
   * @param world le monde
   */
  private writeFile(world: World) {
    const output = createWriteStream(process.env.OUTPUT_FILE, { flags: 'w' });

    const map = world.map;
    output.write(`C - ${map.width} - ${map.height}`);

    world.mountains.forEach((mountain: Mountain) => {
      output.write('\n');
      output.write(`${mountain.type} - ${mountain.x} - ${mountain.y}`);
    });

    world.tresures
      .filter((tresure: Tresure) => tresure.nb > 0)
      .forEach((tresure: Tresure) => {
        output.write('\n');
        output.write(`${tresure.type} - ${tresure.x} - ${tresure.y} - ${tresure.nb}`);
      });

    world.adventurers.forEach((adventurer: Adventurer) => {
      output.write('\n');
      output.write(`${adventurer.type} - ${adventurer.name} - ${adventurer.x} - ${adventurer.y} - ${adventurer.direction} - ${adventurer.nbTresures}`);
    });
  }

  /**
   * Génère le fichier sous forme de tableau
   * @param world le monde
   */
  private writeGraph(world: World) {
    this.spacing = this.findSpacing(world);

    const map = world.map;
    this.output = createWriteStream(process.env.OUTPUT_GRAPH, { flags: 'w' });

    for (let y = 0; y < map.height; y++) {
      for (let x = 0; x < map.width; x++) {
        const cell = map.board[x][y];

        this.lastElement = x + 1 >= map.width;

        if (cell.adventurer) {
          this.outputAdventurer(cell.adventurer);
        } else if (cell.resource) {
          this.outputResource(cell.resource);
        } else {
          this.output.write('.');
          if (!this.lastElement) {
            this.output.write(' '.repeat(this.spacing));
          }
        }
      }

      if (y + 1 < map.height) {
        this.output.write('\n'.repeat(this.gapSize));
      }
    }
  }

  /**
   * Retourne l'espacement entre chaque cellule à afficher
   * @param world le monde
   * @returns
   */
  private findSpacing(world: World): number {
    const maxNameLength = world.adventurers.length > 0 ? Math.max(...world.adventurers.map((adventurer) => adventurer.name.length)) + 2 : 0;
    const maxNbTresuresLength = world.tresures.length > 0 ? Math.max(...world.tresures.map((tresure) => (tresure as Tresure).nb.toString().length)) + 2 : 0;

    return Math.max(maxNameLength, maxNbTresuresLength) + 1;
  }

  /**
   * Génère la représentation d'un aventurier
   * @param adventurer un aventurier
   */
  private outputAdventurer(adventurer: Adventurer) {
    const message = `${adventurer.type}(${adventurer.name})`;
    this.output.write(message);
    if (!this.lastElement) {
      this.output.write(' '.repeat(this.spacing - message.length + 1));
    }
  }

  /**
   * Génère la représentation d'une ressource
   * @param resource une ressource
   */
  private outputResource(resource: Resource) {
    if (resource.type === ResourceType.Enum.TRESURE) {
      const tresure = resource as Tresure;
      const message = `${tresure.type}(${tresure.nb})`;
      this.output.write(message);
      if (!this.lastElement) {
        this.output.write(' '.repeat(this.spacing - message.length + 1));
      }
    } else {
      this.output.write(resource.type);
      if (!this.lastElement) {
        this.output.write(' '.repeat(this.spacing));
      }
    }
  }
}
