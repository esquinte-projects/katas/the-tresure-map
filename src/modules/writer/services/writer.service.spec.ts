import { Adventurer } from '@models/adventurer.model';
import { Cell } from '@models/cell.model';
import { Direction, Movement } from '@models/enums/business';
import { Map } from '@models/map.model';
import { Mountain } from '@models/mountain.model';
import { Tresure } from '@models/tresure.model';
import { World } from '@models/world.model';
import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { LoggerModule } from '@shared/logger';
import * as fs from 'fs';
import { WriterService } from './writer.service';

describe('WriterService', () => {
  let service: WriterService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [WriterService],
      imports: [ConfigModule.forRoot({ envFilePath: 'test/config-test.env' }), LoggerModule],
    }).compile();

    service = moduleRef.get<WriterService>(WriterService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('write', () => {
    it('should output result', () => {
      const world = new World();
      world.map = new Map(3, 4);
      world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

      const tresure1 = new Tresure(1, 0, 2);
      const tresure2 = new Tresure(2, 0, 4);
      world.tresures.push(tresure1, tresure2);

      world.map.board[1][0].resource = tresure1;
      world.map.board[2][0].resource = tresure2;

      const adventurer1 = new Adventurer('Lara', 1, 1, Direction.Enum.SOUTH, [Movement.Enum.FORWARD]);
      const adventurer2 = new Adventurer('JohnDoe', 2, 1, Direction.Enum.NORTH, [Movement.Enum.LEFT]);
      world.adventurers.push(adventurer1, adventurer2);

      world.map.board[1][1].adventurer = adventurer1;
      world.map.board[2][1].adventurer = adventurer2;

      const mountain1 = new Mountain(1, 2);
      const mountain2 = new Mountain(2, 2);
      world.mountains.push(mountain1, mountain2);

      world.map.board[1][2].resource = mountain1;
      world.map.board[2][2].resource = mountain2;

      const mockWrite: jest.Mock = jest.fn();
      const mockCreateWriteStream: jest.SpyInstance = jest.spyOn(fs, 'createWriteStream').mockReturnValue({ write: mockWrite } as any);

      service.write(world);

      expect(mockCreateWriteStream).toHaveBeenCalledTimes(2);
      expect(mockWrite).toHaveBeenCalledTimes(36);

      // Text output
      expect(mockWrite).toHaveBeenNthCalledWith(1, 'C - 3 - 4');
      expect(mockWrite).toHaveBeenNthCalledWith(2, '\n');
      expect(mockWrite).toHaveBeenNthCalledWith(3, 'M - 1 - 2');
      expect(mockWrite).toHaveBeenNthCalledWith(4, '\n');
      expect(mockWrite).toHaveBeenNthCalledWith(5, 'M - 2 - 2');
      expect(mockWrite).toHaveBeenNthCalledWith(6, '\n');
      expect(mockWrite).toHaveBeenNthCalledWith(7, 'T - 1 - 0 - 2');
      expect(mockWrite).toHaveBeenNthCalledWith(8, '\n');
      expect(mockWrite).toHaveBeenNthCalledWith(9, 'T - 2 - 0 - 4');
      expect(mockWrite).toHaveBeenNthCalledWith(10, '\n');
      expect(mockWrite).toHaveBeenNthCalledWith(11, 'A - Lara - 1 - 1 - S - 0');
      expect(mockWrite).toHaveBeenNthCalledWith(12, '\n');
      expect(mockWrite).toHaveBeenNthCalledWith(13, 'A - JohnDoe - 2 - 1 - N - 0');

      // Graph output
      expect(mockWrite).toHaveBeenNthCalledWith(14, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(15, ' '.repeat(10));
      expect(mockWrite).toHaveBeenNthCalledWith(16, 'T(2)');
      expect(mockWrite).toHaveBeenNthCalledWith(17, ' '.repeat(7));
      expect(mockWrite).toHaveBeenNthCalledWith(18, 'T(4)');

      expect(mockWrite).toHaveBeenNthCalledWith(19, '\n'.repeat(2));

      expect(mockWrite).toHaveBeenNthCalledWith(20, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(21, ' '.repeat(10));
      expect(mockWrite).toHaveBeenNthCalledWith(22, 'A(Lara)');
      expect(mockWrite).toHaveBeenNthCalledWith(23, ' '.repeat(4));
      expect(mockWrite).toHaveBeenNthCalledWith(24, 'A(JohnDoe)');

      expect(mockWrite).toHaveBeenNthCalledWith(25, '\n'.repeat(2));

      expect(mockWrite).toHaveBeenNthCalledWith(26, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(27, ' '.repeat(10));
      expect(mockWrite).toHaveBeenNthCalledWith(28, 'M');
      expect(mockWrite).toHaveBeenNthCalledWith(29, ' '.repeat(10));
      expect(mockWrite).toHaveBeenNthCalledWith(30, 'M');

      expect(mockWrite).toHaveBeenNthCalledWith(31, '\n'.repeat(2));

      expect(mockWrite).toHaveBeenNthCalledWith(32, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(33, ' '.repeat(10));
      expect(mockWrite).toHaveBeenNthCalledWith(34, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(35, ' '.repeat(10));
      expect(mockWrite).toHaveBeenNthCalledWith(36, '.');
    });

    it('should output even without resources nor adventurer', () => {
      const world = new World();
      world.map = new Map(3, 4);
      world.map.board = Array.from({ length: world.map.width }, () => Array.from({ length: world.map.height }, () => new Cell()));

      const mockWrite: jest.Mock = jest.fn();
      const mockCreateWriteStream: jest.SpyInstance = jest.spyOn(fs, 'createWriteStream').mockReturnValue({ write: mockWrite } as any);

      service.write(world);

      expect(mockCreateWriteStream).toHaveBeenCalledTimes(2);
      expect(mockWrite).toHaveBeenCalledTimes(24);

      // Text output
      expect(mockWrite).toHaveBeenNthCalledWith(1, 'C - 3 - 4');

      // Graph output
      expect(mockWrite).toHaveBeenNthCalledWith(2, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(3, ' ');
      expect(mockWrite).toHaveBeenNthCalledWith(4, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(5, ' ');
      expect(mockWrite).toHaveBeenNthCalledWith(6, '.');

      expect(mockWrite).toHaveBeenNthCalledWith(7, '\n'.repeat(2));

      expect(mockWrite).toHaveBeenNthCalledWith(8, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(9, ' ');
      expect(mockWrite).toHaveBeenNthCalledWith(10, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(11, ' ');
      expect(mockWrite).toHaveBeenNthCalledWith(12, '.');

      expect(mockWrite).toHaveBeenNthCalledWith(13, '\n'.repeat(2));

      expect(mockWrite).toHaveBeenNthCalledWith(14, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(15, ' ');
      expect(mockWrite).toHaveBeenNthCalledWith(16, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(17, ' ');
      expect(mockWrite).toHaveBeenNthCalledWith(18, '.');

      expect(mockWrite).toHaveBeenNthCalledWith(19, '\n'.repeat(2));

      expect(mockWrite).toHaveBeenNthCalledWith(20, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(21, ' ');
      expect(mockWrite).toHaveBeenNthCalledWith(22, '.');
      expect(mockWrite).toHaveBeenNthCalledWith(23, ' ');
      expect(mockWrite).toHaveBeenNthCalledWith(24, '.');
    });
  });
});
