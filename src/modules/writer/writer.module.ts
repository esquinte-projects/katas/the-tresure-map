import { Module } from '@nestjs/common';
import { WriterService } from './services/writer.service';

@Module({
  providers: [WriterService],
})
export class WriterModule {}
