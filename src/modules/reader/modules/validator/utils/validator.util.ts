import { Direction, Movement, ResourceType } from '@models/enums/business';
import { Dimension } from '@models/interfaces';

/**
 * Classe utilitaire pour les validations
 */
export class ValidatorUtil {
  static readonly digitRegExp = new RegExp(/^\d+$/);
  static readonly quantityRegExp = new RegExp(/^(?!0)\d+$/);

  static readonly resourceTypeRegExp = new RegExp(`^[${ResourceType.getValues().join()}]$`);
  static readonly directionRegExp = new RegExp(`^[${Direction.getValues().join()}]{1}$`);
  static readonly movementRegExp = new RegExp(`^[${Movement.getValues().join()}]+$`);

  static mapProcessed: boolean;

  static mapDimensions: Dimension;

  /**
   * Détermine si la coordonnée X est comprise dans les dimensions de la carte
   * @param coordinate la coordonnée X
   * @returns true si la coordonnée est valide sinon false
   */
  static isXCoordAllowed = (coordinate: string): boolean => +ValidatorUtil.mapDimensions.width - 1 >= +coordinate;

  /**
   * Détermine si la coordonnée Y est comprise dans les dimensions de la carte
   * @param coordinate la coordonnée Y
   * @returns true si la coordonnée est valide sinon false
   */
  static isYCoordAllowed = (coordinate: string): boolean => +ValidatorUtil.mapDimensions.height - 1 >= +coordinate;
}
