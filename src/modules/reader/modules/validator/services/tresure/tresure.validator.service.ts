import { ResourceType } from '@models/enums/business';
import { Injectable } from '@nestjs/common';
import { LoggerService } from '@shared/logger';
import { Validator } from '../../models/validator.interface';
import { ValidatorUtil } from '../../utils/validator.util';

/**
 * Service de validation d'un trésor
 */
@Injectable()
export class TresureValidatorService implements Validator {
  constructor(private loggerService: LoggerService) {}

  isValid([, coordX, coordY, nbOfTresures]: string[]): boolean {
    if (!ValidatorUtil.mapProcessed) {
      this.loggerService.logNoMapProcessed(TresureValidatorService.name);
      throw new Error();
    }

    const isValidCoordX = ValidatorUtil.digitRegExp.test(coordX) && ValidatorUtil.isXCoordAllowed(coordX);
    const isValidCoordY = ValidatorUtil.digitRegExp.test(coordY) && ValidatorUtil.isYCoordAllowed(coordY);
    const isValidNbOfTresures = ValidatorUtil.quantityRegExp.test(nbOfTresures);

    if (isValidCoordX && isValidCoordY && isValidNbOfTresures) {
      return true;
    }

    if (!isValidCoordX) {
      this.loggerService.logInvalidCoordinateX({ coordX, coordY }, ValidatorUtil.mapDimensions, ResourceType.Enum.TRESURE, TresureValidatorService.name);
    }

    if (!isValidCoordY) {
      this.loggerService.logInvalidCoordinateY({ coordX, coordY }, ValidatorUtil.mapDimensions, ResourceType.Enum.TRESURE, TresureValidatorService.name);
    }

    if (!isValidNbOfTresures) {
      this.loggerService.logInvalidNumberOfTresure({ coordX, coordY }, ResourceType.Enum.TRESURE, TresureValidatorService.name);
    }
  }
}
