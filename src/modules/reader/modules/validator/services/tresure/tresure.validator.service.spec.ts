import { Test } from '@nestjs/testing';
import { LoggerModule, LoggerService } from '@shared/logger';
import { ValidatorUtil } from '../../utils/validator.util';
import { TresureValidatorService } from './tresure.validator.service';

describe('TresureValidatorService', () => {
  let service: TresureValidatorService;
  let logger: LoggerService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [TresureValidatorService],
      imports: [LoggerModule],
    }).compile();

    service = moduleRef.get<TresureValidatorService>(TresureValidatorService);
    logger = moduleRef.get<LoggerService>(LoggerService);

    ValidatorUtil.mapProcessed = false;
    ValidatorUtil.mapDimensions = null;
  });

  function initMap() {
    ValidatorUtil.mapProcessed = true;
    ValidatorUtil.mapDimensions = { width: '5', height: '5' };
  }

  describe('isValid', () => {
    it('should return true when tresure is valid', () => {
      initMap();

      let isValid = service.isValid(['T', '0', '0', '1']);
      expect(isValid).toBe(true);

      isValid = service.isValid(['T', '4', '4', '1']);
      expect(isValid).toBe(true);
    });

    it('should log and throw an error when no map processed', () => {
      const mockLogNoMapProcessed = jest.spyOn(logger, 'logNoMapProcessed').mockImplementation();

      let isValid: boolean;

      expect(() => (isValid = service.isValid(['T', '4', '4', '1']))).toThrowError();

      expect(mockLogNoMapProcessed).toHaveBeenCalledTimes(1);
      expect(mockLogNoMapProcessed).toHaveBeenCalledWith('TresureValidatorService');

      expect(isValid).toBeFalsy();
    });

    it('should return false when invalid coordinate X', () => {
      initMap();

      const mockLogInvalidCoordinateX = jest.spyOn(logger, 'logInvalidCoordinateX').mockImplementation();

      let isValid = service.isValid(['T', '-1', '4', '1']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['T', '5', '4', '1']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidCoordinateX).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '4' }, { width: '5', height: '5' }, 'T', 'TresureValidatorService');
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '4' }, { width: '5', height: '5' }, 'T', 'TresureValidatorService');
    });

    it('should return false when invalid coordinate Y', () => {
      initMap();

      const mockLogInvalidCoordinateY = jest.spyOn(logger, 'logInvalidCoordinateY').mockImplementation();

      let isValid = service.isValid(['T', '4', '-1', '1']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['T', '4', '5', '1']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidCoordinateY).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(1, { coordX: '4', coordY: '-1' }, { width: '5', height: '5' }, 'T', 'TresureValidatorService');
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(2, { coordX: '4', coordY: '5' }, { width: '5', height: '5' }, 'T', 'TresureValidatorService');
    });

    it('should return false when invalid number of tresures', () => {
      initMap();

      const mockLogInvalidNumberOfTresure = jest.spyOn(logger, 'logInvalidNumberOfTresure').mockImplementation();

      let isValid = service.isValid(['T', '4', '4', '0']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['T', '4', '4', '-1']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidNumberOfTresure).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidNumberOfTresure).toHaveBeenNthCalledWith(1, { coordX: '4', coordY: '4' }, 'T', 'TresureValidatorService');
      expect(mockLogInvalidNumberOfTresure).toHaveBeenNthCalledWith(2, { coordX: '4', coordY: '4' }, 'T', 'TresureValidatorService');
    });

    it('should return false when invalid coordinate X and Y and number of tresures', () => {
      initMap();

      const mockLogInvalidCoordinateX = jest.spyOn(logger, 'logInvalidCoordinateX').mockImplementation();
      const mockLogInvalidCoordinateY = jest.spyOn(logger, 'logInvalidCoordinateY').mockImplementation();
      const mockLogInvalidNumberOfTresure = jest.spyOn(logger, 'logInvalidNumberOfTresure').mockImplementation();

      let isValid = service.isValid(['T', '-1', '-1', '0']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['T', '5', '5', '-1']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidCoordinateX).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '-1' }, { width: '5', height: '5' }, 'T', 'TresureValidatorService');
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '5' }, { width: '5', height: '5' }, 'T', 'TresureValidatorService');

      expect(mockLogInvalidCoordinateY).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '-1' }, { width: '5', height: '5' }, 'T', 'TresureValidatorService');
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '5' }, { width: '5', height: '5' }, 'T', 'TresureValidatorService');

      expect(mockLogInvalidNumberOfTresure).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidNumberOfTresure).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '-1' }, 'T', 'TresureValidatorService');
      expect(mockLogInvalidNumberOfTresure).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '5' }, 'T', 'TresureValidatorService');
    });
  });
});
