import { Test } from '@nestjs/testing';
import { LoggerModule, LoggerService } from '@shared/logger';
import { ValidatorUtil } from '../../utils/validator.util';
import { MapValidatorService } from './map.validator.service';

describe('MapValidatorService', () => {
  let service: MapValidatorService;
  let logger: LoggerService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [MapValidatorService],
      imports: [LoggerModule],
    }).compile();

    service = moduleRef.get<MapValidatorService>(MapValidatorService);
    logger = moduleRef.get<LoggerService>(LoggerService);

    ValidatorUtil.mapProcessed = false;
    ValidatorUtil.mapDimensions = null;
  });

  describe('isValid', () => {
    it('should return true when map is valid', () => {
      const isValid = service.isValid(['C', '5', '5']);

      expect(isValid).toBe(true);

      expect(ValidatorUtil.mapProcessed).toBe(true);
      expect(ValidatorUtil.mapDimensions).toEqual({ width: '5', height: '5' });
    });

    it('should return false when duplicate map', () => {
      const mockLogDuplicateMap = jest.spyOn(logger, 'logDuplicateMap').mockImplementation();

      let isValid = service.isValid(['C', '5', '5']);
      expect(isValid).toBe(true);

      expect(ValidatorUtil.mapProcessed).toBe(true);
      expect(ValidatorUtil.mapDimensions).toEqual({ width: '5', height: '5' });

      isValid = service.isValid(['C', '6', '6']);
      expect(isValid).toBe(false);

      expect(ValidatorUtil.mapProcessed).toBe(true);
      expect(ValidatorUtil.mapDimensions).toEqual({ width: '5', height: '5' });

      expect(mockLogDuplicateMap).toHaveBeenCalledTimes(1);
      expect(mockLogDuplicateMap).toHaveBeenCalledWith({ width: '6', height: '6' }, 'C', 'MapValidatorService');
    });

    it('should return false when invalid width', () => {
      const mockLogInvalidWidth = jest.spyOn(logger, 'logInvalidWidth').mockImplementation();

      let isValid = service.isValid(['C', '0', '5']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['C', '-1', '5']);
      expect(isValid).toBeFalsy();

      expect(ValidatorUtil.mapProcessed).toBe(false);
      expect(ValidatorUtil.mapDimensions).toBeNull();

      expect(mockLogInvalidWidth).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidWidth).toHaveBeenNthCalledWith(1, { width: '0', height: '5' }, 'C', 'MapValidatorService');
      expect(mockLogInvalidWidth).toHaveBeenNthCalledWith(2, { width: '-1', height: '5' }, 'C', 'MapValidatorService');
    });

    it('should return false when invalid height', () => {
      const mockLogInvalidHeight = jest.spyOn(logger, 'logInvalidHeight').mockImplementation();

      let isValid = service.isValid(['C', '5', '0']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['C', '5', '-1']);
      expect(isValid).toBeFalsy();

      expect(ValidatorUtil.mapProcessed).toBe(false);
      expect(ValidatorUtil.mapDimensions).toBeNull();

      expect(mockLogInvalidHeight).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidHeight).toHaveBeenNthCalledWith(1, { width: '5', height: '0' }, 'C', 'MapValidatorService');
      expect(mockLogInvalidHeight).toHaveBeenNthCalledWith(2, { width: '5', height: '-1' }, 'C', 'MapValidatorService');
    });

    it('should return false when invalid width and height', () => {
      const mockLogInvalidWidth = jest.spyOn(logger, 'logInvalidWidth').mockImplementation();
      const mockLogInvalidHeight = jest.spyOn(logger, 'logInvalidHeight').mockImplementation();

      let isValid = service.isValid(['C', '0', '0']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['C', '-1', '-1']);
      expect(isValid).toBeFalsy();

      expect(ValidatorUtil.mapProcessed).toBe(false);
      expect(ValidatorUtil.mapDimensions).toBeNull();

      expect(mockLogInvalidWidth).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidWidth).toHaveBeenNthCalledWith(1, { width: '0', height: '0' }, 'C', 'MapValidatorService');
      expect(mockLogInvalidWidth).toHaveBeenNthCalledWith(2, { width: '-1', height: '-1' }, 'C', 'MapValidatorService');

      expect(mockLogInvalidHeight).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidHeight).toHaveBeenNthCalledWith(1, { width: '0', height: '0' }, 'C', 'MapValidatorService');
      expect(mockLogInvalidHeight).toHaveBeenNthCalledWith(2, { width: '-1', height: '-1' }, 'C', 'MapValidatorService');
    });
  });
});
