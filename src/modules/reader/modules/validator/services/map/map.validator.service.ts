import { ResourceType } from '@models/enums/business';
import { Injectable } from '@nestjs/common';
import { LoggerService } from '@shared/logger';
import { Validator } from '../../models/validator.interface';
import { ValidatorUtil } from '../../utils/validator.util';

/**
 * Service de validation d'une carte
 */
@Injectable()
export class MapValidatorService implements Validator {
  constructor(private loggerService: LoggerService) {}

  isValid([, width, height]: string[]): boolean {
    if (ValidatorUtil.mapProcessed) {
      this.loggerService.logDuplicateMap({ width, height }, ResourceType.Enum.MAP, MapValidatorService.name);
      return false;
    }

    const isValidWidth = ValidatorUtil.quantityRegExp.test(width);
    const isValidHeight = ValidatorUtil.quantityRegExp.test(height);

    if (isValidWidth && isValidHeight) {
      ValidatorUtil.mapProcessed = true;
      ValidatorUtil.mapDimensions = { width, height };
      return true;
    }

    if (!isValidWidth) {
      this.loggerService.logInvalidWidth({ width, height }, ResourceType.Enum.MAP, MapValidatorService.name);
    }

    if (!isValidHeight) {
      this.loggerService.logInvalidHeight({ width, height }, ResourceType.Enum.MAP, MapValidatorService.name);
    }
  }
}
