import { Test } from '@nestjs/testing';
import { LoggerModule, LoggerService } from '@shared/logger';
import { ValidatorUtil } from '../../utils/validator.util';
import { AdventurerValidatorService } from './adventurer.validator.service';

describe('AdventurerValidatorService', () => {
  let service: AdventurerValidatorService;
  let logger: LoggerService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [AdventurerValidatorService],
      imports: [LoggerModule],
    }).compile();

    service = moduleRef.get<AdventurerValidatorService>(AdventurerValidatorService);
    logger = moduleRef.get<LoggerService>(LoggerService);

    ValidatorUtil.mapProcessed = false;
    ValidatorUtil.mapDimensions = null;
  });

  function initMap() {
    ValidatorUtil.mapProcessed = true;
    ValidatorUtil.mapDimensions = { width: '5', height: '5' };
  }

  describe('isValid', () => {
    it('should return true when adventurer is valid', () => {
      initMap();

      let isValid = service.isValid(['A', 'Lara', '0', '0', 'S', 'ADG']);
      expect(isValid).toBe(true);

      isValid = service.isValid(['A', 'Lara', '4', '4', 'S', 'ADG']);
      expect(isValid).toBe(true);
    });

    it('should log and throw an error when no map processed', () => {
      const mockLogNoMapProcessed = jest.spyOn(logger, 'logNoMapProcessed').mockImplementation();

      let isValid: boolean;

      expect(() => (isValid = service.isValid(['A', 'Lara', '0', '0', 'S', 'ADG']))).toThrowError();

      expect(mockLogNoMapProcessed).toHaveBeenCalledTimes(1);
      expect(mockLogNoMapProcessed).toHaveBeenCalledWith('AdventurerValidatorService');

      expect(isValid).toBeFalsy();
    });

    it('should return false when invalid coordinate X', () => {
      initMap();

      const mockLogInvalidCoordinateX = jest.spyOn(logger, 'logInvalidCoordinateX').mockImplementation();

      let isValid = service.isValid(['A', 'Lara', '-1', '4', 'S', 'ADG']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['A', 'Lara', '5', '4', 'S', 'ADG']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidCoordinateX).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '4' }, { width: '5', height: '5' }, 'A', 'AdventurerValidatorService');
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '4' }, { width: '5', height: '5' }, 'A', 'AdventurerValidatorService');
    });

    it('should return false when invalid coordinate Y', () => {
      initMap();

      const mockLogInvalidCoordinateY = jest.spyOn(logger, 'logInvalidCoordinateY').mockImplementation();

      let isValid = service.isValid(['A', 'Lara', '4', '-1', 'S', 'ADG']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['A', 'Lara', '4', '5', 'S', 'ADG']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidCoordinateY).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(1, { coordX: '4', coordY: '-1' }, { width: '5', height: '5' }, 'A', 'AdventurerValidatorService');
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(2, { coordX: '4', coordY: '5' }, { width: '5', height: '5' }, 'A', 'AdventurerValidatorService');
    });

    it('should return false when invalid direction', () => {
      initMap();

      const mockLogInvalidDirection = jest.spyOn(logger, 'logInvalidDirection').mockImplementation();

      const isValid = service.isValid(['A', 'Lara', '4', '4', 'Z', 'ADG']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidDirection).toHaveBeenCalledTimes(1);
      expect(mockLogInvalidDirection).toHaveBeenCalledWith({ coordX: '4', coordY: '4' }, 'A', 'AdventurerValidatorService');
    });

    it('should return false when invalid movements', () => {
      initMap();

      const mockLogInvalidMovements = jest.spyOn(logger, 'logInvalidMovements').mockImplementation();

      const isValid = service.isValid(['A', 'Lara', '4', '4', 'Z', 'ADGZ']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidMovements).toHaveBeenCalledTimes(1);
      expect(mockLogInvalidMovements).toHaveBeenCalledWith({ coordX: '4', coordY: '4' }, 'A', 'AdventurerValidatorService');
    });

    it('should return false when invalid coordinate X and Y, direction and movements', () => {
      initMap();

      const mockLogInvalidCoordinateX = jest.spyOn(logger, 'logInvalidCoordinateX').mockImplementation();
      const mockLogInvalidCoordinateY = jest.spyOn(logger, 'logInvalidCoordinateY').mockImplementation();
      const mockLogInvalidDirection = jest.spyOn(logger, 'logInvalidDirection').mockImplementation();
      const mockLogInvalidMovements = jest.spyOn(logger, 'logInvalidMovements').mockImplementation();

      let isValid = service.isValid(['A', 'Lara', '-1', '-1', 'Z', 'ADGZ']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['A', 'Lara', '5', '5', 'Z', 'ADGZ']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidCoordinateX).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '-1' }, { width: '5', height: '5' }, 'A', 'AdventurerValidatorService');
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '5' }, { width: '5', height: '5' }, 'A', 'AdventurerValidatorService');

      expect(mockLogInvalidCoordinateY).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '-1' }, { width: '5', height: '5' }, 'A', 'AdventurerValidatorService');
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '5' }, { width: '5', height: '5' }, 'A', 'AdventurerValidatorService');

      expect(mockLogInvalidDirection).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidDirection).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '-1' }, 'A', 'AdventurerValidatorService');
      expect(mockLogInvalidDirection).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '5' }, 'A', 'AdventurerValidatorService');

      expect(mockLogInvalidMovements).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidMovements).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '-1' }, 'A', 'AdventurerValidatorService');
      expect(mockLogInvalidMovements).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '5' }, 'A', 'AdventurerValidatorService');
    });
  });
});
