import { ResourceType } from '@models/enums/business';
import { Injectable } from '@nestjs/common';
import { LoggerService } from '@shared/logger';
import { Validator } from '../../models/validator.interface';
import { ValidatorUtil } from '../../utils/validator.util';

/**
 * Service de validation d'un aventurier
 */
@Injectable()
export class AdventurerValidatorService implements Validator {
  constructor(private loggerService: LoggerService) {}

  isValid([, , coordX, coordY, direction, movements]: string[]): boolean {
    if (!ValidatorUtil.mapProcessed) {
      this.loggerService.logNoMapProcessed(AdventurerValidatorService.name);
      throw new Error();
    }

    const isValidCoordX = ValidatorUtil.digitRegExp.test(coordX) && ValidatorUtil.isXCoordAllowed(coordX);
    const isValidCoordY = ValidatorUtil.digitRegExp.test(coordY) && ValidatorUtil.isYCoordAllowed(coordY);
    const isValidDirection = ValidatorUtil.directionRegExp.test(direction);
    const isValidMovements = ValidatorUtil.movementRegExp.test(movements);

    if (isValidCoordX && isValidCoordY && isValidDirection && isValidMovements) {
      return true;
    }

    if (!isValidCoordX) {
      this.loggerService.logInvalidCoordinateX({ coordX, coordY }, ValidatorUtil.mapDimensions, ResourceType.Enum.ADVENTURER, AdventurerValidatorService.name);
    }

    if (!isValidCoordY) {
      this.loggerService.logInvalidCoordinateY({ coordX, coordY }, ValidatorUtil.mapDimensions, ResourceType.Enum.ADVENTURER, AdventurerValidatorService.name);
    }

    if (!isValidDirection) {
      this.loggerService.logInvalidDirection({ coordX, coordY }, ResourceType.Enum.ADVENTURER, AdventurerValidatorService.name);
    }

    if (!isValidMovements) {
      this.loggerService.logInvalidMovements({ coordX, coordY }, ResourceType.Enum.ADVENTURER, AdventurerValidatorService.name);
    }
  }
}
