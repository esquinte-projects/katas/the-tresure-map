import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { LoggerModule } from '@shared/logger';
import { AdventurerValidatorService } from './adventurer/adventurer.validator.service';
import { MapValidatorService } from './map/map.validator.service';
import { MountainValidatorService } from './mountain/mountain.validator.service';
import { TresureValidatorService } from './tresure/tresure.validator.service';
import { ValidatorService } from './validator.service';

describe('ValidatorService', () => {
  let service: ValidatorService;
  let adventurer: AdventurerValidatorService;
  let map: MapValidatorService;
  let mountain: MountainValidatorService;
  let tresure: TresureValidatorService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [ValidatorService, AdventurerValidatorService, MapValidatorService, MountainValidatorService, TresureValidatorService],
      imports: [ConfigModule.forRoot({ envFilePath: 'test/config-test.env' }), LoggerModule],
    }).compile();

    service = moduleRef.get<ValidatorService>(ValidatorService);
    adventurer = moduleRef.get<AdventurerValidatorService>(AdventurerValidatorService);
    map = moduleRef.get<MapValidatorService>(MapValidatorService);
    mountain = moduleRef.get<MountainValidatorService>(MountainValidatorService);
    tresure = moduleRef.get<TresureValidatorService>(TresureValidatorService);
  });

  describe('isValidLine', () => {
    it('should return false when invalid resource type', () => {
      const mockAdventurerIsValid = jest.spyOn(adventurer, 'isValid').mockImplementation();
      const mockMapIsValid = jest.spyOn(map, 'isValid').mockImplementation();
      const mockMountainIsValid = jest.spyOn(mountain, 'isValid').mockImplementation();
      const mockTresureIsValid = jest.spyOn(tresure, 'isValid').mockImplementation();

      const isValid = service.isValidLine('Z - 5 - 5');

      expect(isValid).toBe(false);

      expect(mockAdventurerIsValid).not.toHaveBeenCalled();
      expect(mockMapIsValid).not.toHaveBeenCalled();
      expect(mockMountainIsValid).not.toHaveBeenCalled();
      expect(mockTresureIsValid).not.toHaveBeenCalled();
    });

    describe('adventurer', () => {
      it('should return true when valid adventurer', () => {
        const mockAdventurerIsValid = jest.spyOn(adventurer, 'isValid').mockReturnValue(true);
        const mockMapIsValid = jest.spyOn(map, 'isValid').mockImplementation();
        const mockMountainIsValid = jest.spyOn(mountain, 'isValid').mockImplementation();
        const mockTresureIsValid = jest.spyOn(tresure, 'isValid').mockImplementation();

        const isValid = service.isValidLine('A - Lara - 5 - 5 - N - ADG');

        expect(isValid).toBe(true);

        expect(mockAdventurerIsValid).toHaveBeenCalledTimes(1);
        expect(mockMapIsValid).not.toHaveBeenCalled();
        expect(mockMountainIsValid).not.toHaveBeenCalled();
        expect(mockTresureIsValid).not.toHaveBeenCalled();
      });

      it('should return false when invalid adventurer', () => {
        const mockAdventurerIsValid = jest.spyOn(adventurer, 'isValid').mockReturnValue(false);
        const mockMapIsValid = jest.spyOn(map, 'isValid').mockImplementation();
        const mockMountainIsValid = jest.spyOn(mountain, 'isValid').mockImplementation();
        const mockTresureIsValid = jest.spyOn(tresure, 'isValid').mockImplementation();

        const isValid = service.isValidLine('A - Lara - 5 - 5 - N - ADG');

        expect(isValid).toBe(false);

        expect(mockAdventurerIsValid).toHaveBeenCalledTimes(1);
        expect(mockMapIsValid).not.toHaveBeenCalled();
        expect(mockMountainIsValid).not.toHaveBeenCalled();
        expect(mockTresureIsValid).not.toHaveBeenCalled();
      });
    });

    describe('map', () => {
      it('should return true when valid map', () => {
        const mockAdventurerIsValid = jest.spyOn(adventurer, 'isValid').mockImplementation();
        const mockMapIsValid = jest.spyOn(map, 'isValid').mockReturnValue(true);
        const mockMountainIsValid = jest.spyOn(mountain, 'isValid').mockImplementation();
        const mockTresureIsValid = jest.spyOn(tresure, 'isValid').mockImplementation();

        const isValid = service.isValidLine('C - 5 - 5');

        expect(isValid).toBe(true);

        expect(mockAdventurerIsValid).not.toHaveBeenCalled();
        expect(mockMapIsValid).toHaveBeenCalledTimes(1);
        expect(mockMountainIsValid).not.toHaveBeenCalled();
        expect(mockTresureIsValid).not.toHaveBeenCalled();
      });

      it('should return false when invalid map', () => {
        const mockAdventurerIsValid = jest.spyOn(adventurer, 'isValid').mockImplementation();
        const mockMapIsValid = jest.spyOn(map, 'isValid').mockReturnValue(false);
        const mockMountainIsValid = jest.spyOn(mountain, 'isValid').mockImplementation();
        const mockTresureIsValid = jest.spyOn(tresure, 'isValid').mockImplementation();

        const isValid = service.isValidLine('C - 5 - 5');

        expect(isValid).toBe(false);

        expect(mockAdventurerIsValid).not.toHaveBeenCalled();
        expect(mockMapIsValid).toHaveBeenCalledTimes(1);
        expect(mockMountainIsValid).not.toHaveBeenCalled();
        expect(mockTresureIsValid).not.toHaveBeenCalled();
      });
    });

    describe('mountain', () => {
      it('should return true when valid mountain', () => {
        const mockAdventurerIsValid = jest.spyOn(adventurer, 'isValid').mockImplementation();
        const mockMapIsValid = jest.spyOn(map, 'isValid').mockImplementation();
        const mockMountainIsValid = jest.spyOn(mountain, 'isValid').mockReturnValue(true);
        const mockTresureIsValid = jest.spyOn(tresure, 'isValid').mockImplementation();

        const isValid = service.isValidLine('M - 5 - 5');

        expect(isValid).toBe(true);

        expect(mockAdventurerIsValid).not.toHaveBeenCalled();
        expect(mockMapIsValid).not.toHaveBeenCalled();
        expect(mockMountainIsValid).toHaveBeenCalledTimes(1);
        expect(mockTresureIsValid).not.toHaveBeenCalled();
      });

      it('should return false when invalid mountain', () => {
        const mockAdventurerIsValid = jest.spyOn(adventurer, 'isValid').mockImplementation();
        const mockMapIsValid = jest.spyOn(map, 'isValid').mockImplementation();
        const mockMountainIsValid = jest.spyOn(mountain, 'isValid').mockReturnValue(false);
        const mockTresureIsValid = jest.spyOn(tresure, 'isValid').mockImplementation();

        const isValid = service.isValidLine('M - 5 - 5');

        expect(isValid).toBe(false);

        expect(mockAdventurerIsValid).not.toHaveBeenCalled();
        expect(mockMapIsValid).not.toHaveBeenCalled();
        expect(mockMountainIsValid).toHaveBeenCalledTimes(1);
        expect(mockTresureIsValid).not.toHaveBeenCalled();
      });
    });

    describe('tresure', () => {
      it('should return true when valid tresure', () => {
        const mockAdventurerIsValid = jest.spyOn(adventurer, 'isValid').mockImplementation();
        const mockMapIsValid = jest.spyOn(map, 'isValid').mockImplementation();
        const mockMountainIsValid = jest.spyOn(mountain, 'isValid').mockImplementation();
        const mockTresureIsValid = jest.spyOn(tresure, 'isValid').mockReturnValue(true);

        const isValid = service.isValidLine('T - 5 - 5 - 2');

        expect(isValid).toBe(true);

        expect(mockAdventurerIsValid).not.toHaveBeenCalled();
        expect(mockMapIsValid).not.toHaveBeenCalled();
        expect(mockMountainIsValid).not.toHaveBeenCalled();
        expect(mockTresureIsValid).toHaveBeenCalledTimes(1);
      });

      it('should return false when invalid tresure', () => {
        const mockAdventurerIsValid = jest.spyOn(adventurer, 'isValid').mockImplementation();
        const mockMapIsValid = jest.spyOn(map, 'isValid').mockImplementation();
        const mockMountainIsValid = jest.spyOn(mountain, 'isValid').mockImplementation();
        const mockTresureIsValid = jest.spyOn(tresure, 'isValid').mockReturnValue(false);

        const isValid = service.isValidLine('T - 5 - 5 - 2');

        expect(isValid).toBe(false);

        expect(mockAdventurerIsValid).not.toHaveBeenCalled();
        expect(mockMapIsValid).not.toHaveBeenCalled();
        expect(mockMountainIsValid).not.toHaveBeenCalled();
        expect(mockTresureIsValid).toHaveBeenCalledTimes(1);
      });
    });
  });
});
