import { ResourceType } from '@models/enums/business';
import { Injectable } from '@nestjs/common';
import { LoggerService } from '@shared/logger';
import { Validator } from '../../models/validator.interface';
import { ValidatorUtil } from '../../utils/validator.util';

/**
 * Service de validation d'une montagne
 */
@Injectable()
export class MountainValidatorService implements Validator {
  constructor(private loggerService: LoggerService) {}

  isValid([, coordX, coordY]: string[]): boolean {
    if (!ValidatorUtil.mapProcessed) {
      this.loggerService.logNoMapProcessed(MountainValidatorService.name);
      throw new Error();
    }

    const isValidCoordX = ValidatorUtil.digitRegExp.test(coordX) && ValidatorUtil.isXCoordAllowed(coordX);
    const isValidCoordY = ValidatorUtil.digitRegExp.test(coordY) && ValidatorUtil.isYCoordAllowed(coordY);

    if (isValidCoordX && isValidCoordY) {
      return true;
    }

    if (!isValidCoordX) {
      this.loggerService.logInvalidCoordinateX({ coordX, coordY }, ValidatorUtil.mapDimensions, ResourceType.Enum.MOUNTAIN, MountainValidatorService.name);
    }

    if (!isValidCoordY) {
      this.loggerService.logInvalidCoordinateY({ coordX, coordY }, ValidatorUtil.mapDimensions, ResourceType.Enum.MOUNTAIN, MountainValidatorService.name);
    }
  }
}
