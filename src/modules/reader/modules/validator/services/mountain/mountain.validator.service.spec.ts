import { Test } from '@nestjs/testing';
import { LoggerModule, LoggerService } from '@shared/logger';
import { ValidatorUtil } from '../../utils/validator.util';
import { MountainValidatorService } from './mountain.validator.service';

describe('MountainValidatorService', () => {
  let service: MountainValidatorService;
  let logger: LoggerService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [MountainValidatorService],
      imports: [LoggerModule],
    }).compile();

    service = moduleRef.get<MountainValidatorService>(MountainValidatorService);
    logger = moduleRef.get<LoggerService>(LoggerService);

    ValidatorUtil.mapProcessed = false;
    ValidatorUtil.mapDimensions = null;
  });

  function initMap() {
    ValidatorUtil.mapProcessed = true;
    ValidatorUtil.mapDimensions = { width: '5', height: '5' };
  }

  describe('isValid', () => {
    it('should return true when mountain is valid', () => {
      initMap();

      let isValid = service.isValid(['M', '0', '0']);
      expect(isValid).toBe(true);

      isValid = service.isValid(['M', '4', '4']);
      expect(isValid).toBe(true);
    });

    it('should log and throw an error when no map processed', () => {
      const mockLogNoMapProcessed = jest.spyOn(logger, 'logNoMapProcessed').mockImplementation();

      let isValid: boolean;

      expect(() => (isValid = service.isValid(['M', '4', '4']))).toThrowError();

      expect(mockLogNoMapProcessed).toHaveBeenCalledTimes(1);
      expect(mockLogNoMapProcessed).toHaveBeenCalledWith('MountainValidatorService');

      expect(isValid).toBeFalsy();
    });

    it('should return false when invalid coordinate X', () => {
      initMap();

      const mockLogInvalidCoordinateX = jest.spyOn(logger, 'logInvalidCoordinateX').mockImplementation();

      let isValid = service.isValid(['M', '-1', '4']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['M', '5', '4']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidCoordinateX).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '4' }, { width: '5', height: '5' }, 'M', 'MountainValidatorService');
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '4' }, { width: '5', height: '5' }, 'M', 'MountainValidatorService');
    });

    it('should return false when invalid coordinate Y', () => {
      initMap();

      const mockLogInvalidCoordinateY = jest.spyOn(logger, 'logInvalidCoordinateY').mockImplementation();

      let isValid = service.isValid(['M', '4', '-1']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['M', '4', '5']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidCoordinateY).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(1, { coordX: '4', coordY: '-1' }, { width: '5', height: '5' }, 'M', 'MountainValidatorService');
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(2, { coordX: '4', coordY: '5' }, { width: '5', height: '5' }, 'M', 'MountainValidatorService');
    });

    it('should return false when invalid coordinate X and Y', () => {
      initMap();

      const mockLogInvalidCoordinateX = jest.spyOn(logger, 'logInvalidCoordinateX').mockImplementation();
      const mockLogInvalidCoordinateY = jest.spyOn(logger, 'logInvalidCoordinateY').mockImplementation();

      let isValid = service.isValid(['M', '-1', '-1']);
      expect(isValid).toBeFalsy();

      isValid = service.isValid(['M', '5', '5']);
      expect(isValid).toBeFalsy();

      expect(mockLogInvalidCoordinateX).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '-1' }, { width: '5', height: '5' }, 'M', 'MountainValidatorService');
      expect(mockLogInvalidCoordinateX).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '5' }, { width: '5', height: '5' }, 'M', 'MountainValidatorService');

      expect(mockLogInvalidCoordinateY).toHaveBeenCalledTimes(2);
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(1, { coordX: '-1', coordY: '-1' }, { width: '5', height: '5' }, 'M', 'MountainValidatorService');
      expect(mockLogInvalidCoordinateY).toHaveBeenNthCalledWith(2, { coordX: '5', coordY: '5' }, { width: '5', height: '5' }, 'M', 'MountainValidatorService');
    });
  });
});
