import { ResourceType } from '@models/enums/business';
import { Injectable } from '@nestjs/common';
import { ValidatorUtil } from '../utils/validator.util';
import { AdventurerValidatorService } from './adventurer/adventurer.validator.service';
import { MapValidatorService } from './map/map.validator.service';
import { MountainValidatorService } from './mountain/mountain.validator.service';
import { TresureValidatorService } from './tresure/tresure.validator.service';

/**
 * Service de validation
 */
@Injectable()
export class ValidatorService {
  constructor(
    private adventurerValidatorService: AdventurerValidatorService,
    private mapValidatorService: MapValidatorService,
    private mountainValidatorService: MountainValidatorService,
    private tresureValidatorService: TresureValidatorService,
  ) {}

  /**
   * Détermine si la ligne est valide
   * @param line une ligne du fichier
   * @returns true si la ligne est validée sinon false
   */
  isValidLine(line: string): boolean {
    let isValid = false;

    const firstLetter = line[0];
    if (!this.isResourceTypeValid(firstLetter)) {
      return isValid;
    }

    const lineFrags = line.split(process.env.INPUT_SEPARATOR);

    if (firstLetter === ResourceType.Enum.MAP) {
      isValid = this.mapValidatorService.isValid(lineFrags);
    }

    if (firstLetter === ResourceType.Enum.MOUNTAIN) {
      isValid = this.mountainValidatorService.isValid(lineFrags);
    }

    if (firstLetter === ResourceType.Enum.TRESURE) {
      isValid = this.tresureValidatorService.isValid(lineFrags);
    }

    if (firstLetter === ResourceType.Enum.ADVENTURER) {
      isValid = this.adventurerValidatorService.isValid(lineFrags);
    }

    return isValid;
  }

  /**
   * Détermine si la première lettre de la ligne est valide
   * @param firstLetter la première lettre
   * @returns true si la première lettre est validé sinon false
   */
  private isResourceTypeValid(firstLetter: string): boolean {
    return ValidatorUtil.resourceTypeRegExp.test(firstLetter);
  }
}
