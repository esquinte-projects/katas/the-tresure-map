import { Module } from '@nestjs/common';
import { AdventurerValidatorService } from './services/adventurer/adventurer.validator.service';
import { MapValidatorService } from './services/map/map.validator.service';
import { MountainValidatorService } from './services/mountain/mountain.validator.service';
import { TresureValidatorService } from './services/tresure/tresure.validator.service';
import { ValidatorService } from './services/validator.service';

@Module({
  providers: [AdventurerValidatorService, MapValidatorService, MountainValidatorService, TresureValidatorService, ValidatorService],
  exports: [ValidatorService],
})
export class ValidatorModule {}
