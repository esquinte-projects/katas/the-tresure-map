/**
 * Interface représentant un validator
 */
export interface Validator {
  /**
   * Détermine si la ligne de donnée est valide
   * @param lineFrags les données de la ligne
   */
  isValid(lineFrags: string[]): boolean;
}
