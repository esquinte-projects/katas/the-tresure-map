import { ReadStreamEvent } from '@models/enums/technical';
import { Injectable } from '@nestjs/common';
import { LoggerService } from '@shared/logger';
import { createReadStream, ReadStream } from 'fs';
import { createInterface } from 'readline';
import { ValidatorService } from '../modules/validator';

/**
 * Service de lecture d'un fichier
 */
@Injectable()
export class FileReaderService {
  constructor(private validatorService: ValidatorService, private loggerService: LoggerService) {}

  /**
   * Extrait les données validées du fichier ligne par ligne
   * @param fileName le nom du fichier
   * @returns une promesse composée des données validées
   */
  async parse(fileName: string): Promise<string[]> {
    try {
      return await this.readLineByLine(fileName);
    } catch (err) {
      if (err.message) {
        this.loggerService.logError(err, FileReaderService.name);
      }
      process.exit(0);
    }
  }

  /**
   * Lis le fichier ligne par ligne
   * @param fileName le nom du fichier
   * @returns une promesse composée des données validées
   */
  private async readLineByLine(fileName: string): Promise<string[]> {
    this.loggerService.logInfo(`Reading file located in "${fileName}"...`, FileReaderService.name);
    const start = process.hrtime.bigint();

    const input = await this.createReadStreamSafe(fileName).catch((err) => {
      throw new Error(err.message);
    });

    const rl = createInterface({
      input: input as ReadStream,
      crlfDelay: Infinity,
    });

    const lines: string[] = [];
    for await (const line of rl) {
      if (this.validatorService.isValidLine(line)) {
        lines.push(line);
      }
    }

    const end = process.hrtime.bigint();

    this.loggerService.logInfo(`Read file in ${(end - start) / BigInt(1e3)} microseconds`, FileReaderService.name);

    if (lines.length === 0) {
      throw new Error('No valid lines in the input file');
    }

    return lines;
  }

  /**
   * Crée un stream en lecture du fichier
   * @param filename le nom du fichier
   * @returns une promesse composée du stream en lecture
   */
  private createReadStreamSafe(filename: string): Promise<ReadStream> {
    return new Promise((resolve, reject) => {
      const fileStream = createReadStream(filename);

      fileStream.on(ReadStreamEvent.ERROR, (err: Error) => reject(err)).on(ReadStreamEvent.OPEN, () => resolve(fileStream));
    });
  }
}
