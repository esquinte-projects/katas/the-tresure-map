import { ConfigModule } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { LoggerModule, LoggerService } from '@shared/logger';
import { ValidatorModule, ValidatorService } from '../modules/validator';
import { FileReaderService } from './file-reader.service';

describe('FileReaderService', () => {
  let service: FileReaderService;
  let validator: ValidatorService;
  let logger: LoggerService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [FileReaderService],
      imports: [ConfigModule.forRoot({ envFilePath: 'test/config-test.env' }), ValidatorModule, LoggerModule],
    }).compile();

    service = moduleRef.get<FileReaderService>(FileReaderService);
    validator = moduleRef.get<ValidatorService>(ValidatorService);
    logger = moduleRef.get<LoggerService>(LoggerService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('parse', () => {
    it('should parse file', async () => {
      const mockIsValid = jest.spyOn(validator, 'isValidLine').mockReturnValue(true);

      const lines = await service.parse(process.env.VALID_FILE);

      expect(lines).not.toBeNull();
      expect(lines).toHaveLength(4);

      expect(lines[0]).toBe('C - 5 - 5');
      expect(lines[1]).toBe('M - 0 - 1');
      expect(lines[2]).toBe('T - 1 - 2 - 2');
      expect(lines[3]).toBe('A - John - 3 - 3 - N - ADGA');

      expect(mockIsValid).toHaveBeenCalledTimes(4);
    });

    it('should throw error when no valid lines', async () => {
      const mockIsValid = jest.spyOn(validator, 'isValidLine').mockReturnValue(false);
      const mockLogError = jest.spyOn(logger, 'logError').mockImplementation();
      const mockExit = jest.spyOn(process, 'exit').mockImplementation();

      const lines = await service.parse(process.env.VALID_FILE);

      expect(lines).toBeUndefined();

      expect(mockLogError).toHaveBeenCalledTimes(1);
      expect(mockLogError).toHaveBeenCalledWith(new Error('No valid lines in the input file'), 'FileReaderService');

      expect(mockExit).toHaveBeenCalledTimes(1);
      expect(mockExit).toHaveBeenCalledWith(0);

      expect(mockIsValid).toHaveBeenCalledTimes(4);
    });

    it('should not log error when no message', async () => {
      const mockIsValid = jest.spyOn(validator, 'isValidLine').mockImplementation(() => {
        throw new Error();
      });
      const mockLogError = jest.spyOn(logger, 'logError').mockImplementation();
      const mockExit = jest.spyOn(process, 'exit').mockImplementation();

      const lines = await service.parse(process.env.VALID_FILE);

      expect(lines).toBeUndefined();

      expect(mockLogError).not.toHaveBeenCalled();

      expect(mockExit).toHaveBeenCalledTimes(1);
      expect(mockExit).toHaveBeenCalledWith(0);

      expect(mockIsValid).toHaveBeenCalledTimes(1);
    });

    it('should throw error when missing file', async () => {
      const mockLogError = jest.spyOn(logger, 'logError').mockImplementation();
      const mockExit = jest.spyOn(process, 'exit').mockImplementation();

      const lines = await service.parse(process.env.MISSING_FILE);

      expect(lines).toBeUndefined();

      expect(mockLogError).toHaveBeenCalledTimes(1);
      expect(mockLogError).toHaveBeenCalledWith(new Error("ENOENT: no such file or directory, open 'test/input/missing.txt'"), FileReaderService.name);

      expect(mockExit).toHaveBeenCalledTimes(1);
      expect(mockExit).toHaveBeenCalledWith(0);
    });
  });
});
