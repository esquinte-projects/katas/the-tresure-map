import { Module } from '@nestjs/common';
import { ValidatorModule } from './modules/validator';
import { FileReaderService } from './services/file-reader.service';

@Module({
  imports: [ValidatorModule],
  providers: [FileReaderService],
})
export class ReaderModule {}
