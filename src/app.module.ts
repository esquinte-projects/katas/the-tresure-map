import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { BuilderModule } from './modules/builder';
import { PlayModule } from './modules/play';
import { ReaderModule } from './modules/reader';
import { WriterModule } from './modules/writer';
import { LoggerModule } from './shared/logger';

@Module({
  imports: [ConfigModule.forRoot({ envFilePath: 'config.env' }), LoggerModule, ReaderModule, BuilderModule, PlayModule, WriterModule],
})
export class AppModule {}
