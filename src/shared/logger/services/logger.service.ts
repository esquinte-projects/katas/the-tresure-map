import { Direction, Movement, ResourceType } from '@models/enums/business';
import { CoordinateEnum, DimensionEnum } from '@models/enums/technical';
import { Coordinate, Dimension } from '@models/interfaces';
import { Injectable, Logger } from '@nestjs/common';

/**
 * Service de log
 */
@Injectable()
export class LoggerService {
  private logger = new Logger();

  /**
   * Log un messsage
   * @param message le message
   * @param context le contexte
   */
  logInfo(message: string, context: string) {
    this.logger.log(message, context);
  }

  /**
   * Log une erreur
   * @param message le message
   * @param context le contexte
   */
  logError(message: string, context: string) {
    this.logger.error('Error encoutered!', context);
    this.logger.error(message, context);
  }

  /************************************************************************/
  /*                              Coordinate                              */
  /************************************************************************/
  /**
   * Log un avertissement sur la position d'une coordonnée en X
   * @param coordinate les coordonnées de la ressource
   * @param dimension les dimensions de la carte
   * @param resourceType le type de la ressource
   * @param context le contexte
   */
  logInvalidCoordinateX(coordinate: Coordinate, dimension: Dimension, resourceType: ResourceType.Enum, context: string) {
    this.logInvalidCoordinate(CoordinateEnum.X, coordinate, +dimension.width - 1, resourceType, context);
  }

  /**
   * Log un avertissement sur la position d'une coordoonée en Y
   * @param coordinate les coordonnées de la ressource
   * @param dimension les dimensions de la carte
   * @param resourceType le type de la ressource
   * @param context le contexte
   */
  logInvalidCoordinateY(coordinate: Coordinate, dimension: Dimension, resourceType: ResourceType.Enum, context: string) {
    this.logInvalidCoordinate(CoordinateEnum.Y, coordinate, +dimension.height - 1, resourceType, context);
  }

  /**
   * Log un avertissement sur la position d'une coordonnée
   * @param coordinateType le type de la coordonnée
   * @param param1 les coordonnées de la ressource
   * @param dimension les dimensions de la carte
   * @param resourceType le type de la ressource
   * @param context le contexte
   */
  private logInvalidCoordinate(coordinateType: string, { coordX, coordY }: Coordinate, dimension: number, resourceType: ResourceType.Enum, context: string) {
    const resourceTypeKey = ResourceType.getResourceTypeKeyFromValue(resourceType);

    this.logger.warn(`Invalid coordinate ${coordinateType}`, context);
    this.logger.warn(`The value must be between 0 and ${dimension}`, context);
    this.logger.warn(`Skipping this ${resourceTypeKey} with coordinate X:${coordX} ; Y:${coordY}`, context);
  }

  /************************************************************************/
  /*                              Dimension                               */
  /************************************************************************/
  /**
   * Log un avertissement sur la longueur d'une dimension
   * @param dimension les dimensions de la carte
   * @param resourceType le type de la ressource
   * @param context le contexte
   */
  logInvalidWidth(dimension: Dimension, resourceType: ResourceType.Enum, context: string) {
    this.logInvalidDimension(DimensionEnum.WIDTH, dimension, resourceType, context);
  }

  /**
   * Log un avertissement sur la hauteur d'une dimension
   * @param dimension les dimensions de la carte
   * @param resourceType le type de la ressource
   * @param context le contexte
   */
  logInvalidHeight(dimension: Dimension, resourceType: ResourceType.Enum, context: string) {
    this.logInvalidDimension(DimensionEnum.HEIGHT, dimension, resourceType, context);
  }

  /**
   * Log un avertissement sur la longueur d'une dimension
   * @param dimensionType le type de dimension
   * @param param1 les dimensions de la carte
   * @param resourceType le type de la ressource
   * @param context le contexte
   */
  private logInvalidDimension(dimensionType: string, { width, height }: Dimension, resourceType: ResourceType.Enum, context: string) {
    const resourceTypeKey = ResourceType.getResourceTypeKeyFromValue(resourceType);

    this.logger.warn(`Invalid ${dimensionType}`, context);
    this.logger.warn('The value must be at least 1', context);
    this.logger.warn(`Skipping this ${resourceTypeKey} with width:${width} and height:${height}`, context);
  }

  /************************************************************************/
  /*                              Map                                     */
  /************************************************************************/
  /**
   * Log un avertissement sur la duplication de carte
   * @param param0 les dimensions de la carte
   * @param resourceType le type de ressource
   * @param context le contexte
   */
  logDuplicateMap({ width, height }: Dimension, resourceType: ResourceType.Enum, context: string) {
    const resourceTypeKey = ResourceType.getResourceTypeKeyFromValue(resourceType);

    this.logger.warn(`A ${resourceTypeKey} has already been processed`, context);
    this.logger.warn(`Skipping this one with width:${width} and height:${height}`, context);
  }

  /**
   * Log une erreur sur la génération de la carte
   * @param context le contexte
   */
  logNoMapProcessed(context: string) {
    this.logger.error('There is no map in the input file', context);
    this.logger.error('Please add one as the first line of the file', context);
    this.logger.error('Syntax : C - {width} - {height}', context);
  }

  /************************************************************************/
  /*                              Tresure                                 */
  /************************************************************************/
  /**
   * Log un avertissement sur le nombre de trésors
   * @param param0 les coordonnées de la ressource
   * @param resourceType le type de ressource
   * @param context le contexte
   */
  logInvalidNumberOfTresure({ coordX, coordY }: Coordinate, resourceType: ResourceType.Enum, context: string) {
    const resourceTypeKey = ResourceType.getResourceTypeKeyFromValue(resourceType);

    this.logger.warn('Invalid number of tresures', context);
    this.logger.warn('The value must be at least 1', context);
    this.logger.warn(`Skipping this ${resourceTypeKey} with coordinate X:${coordX} ; Y:${coordY}`, context);
  }

  /************************************************************************/
  /*                              Adventurer                              */
  /************************************************************************/
  /**
   * Log un avertissement sur la direction d'un aventurier
   * @param param0 les coordonnées de l'aventurier
   * @param resourceType le type de ressource
   * @param context le contexte
   */
  logInvalidDirection({ coordX, coordY }: Coordinate, resourceType: ResourceType.Enum, context: string) {
    const resourceTypeKey = ResourceType.getResourceTypeKeyFromValue(resourceType);

    this.logger.warn('Invalid direction', context);
    this.logger.warn(`The value must be any of ${Direction.getValues()}`, context);
    this.logger.warn(`Skipping this ${resourceTypeKey} with coordinate X:${coordX} ; Y:${coordY}`, context);
  }

  /**
   * Log un avertissement sur les mouvements d'un aventurier
   * @param param0 les coordonnées de l'aventurier
   * @param resourceType le type de ressource
   * @param context le contexte
   */
  logInvalidMovements({ coordX, coordY }: Coordinate, resourceType: ResourceType.Enum, context: string) {
    const resourceTypeKey = ResourceType.getResourceTypeKeyFromValue(resourceType);

    this.logger.warn('Invalid movements', context);
    this.logger.warn(`The values must be any of ${Movement.getValues()}`, context);
    this.logger.warn(`Skipping this ${resourceTypeKey} with coordinate X:${coordX} ; Y:${coordY}`, context);
  }

  /************************************************************************/
  /*                              Play                                    */
  /************************************************************************/
  /**
   * Log un avertissement sur un mouvement dépassant les limites de la carte
   * @param currCoord les coordonnées courantes du joueur
   * @param nextCoord les coordonnées suivantes du joueur
   * @param playerName le nom du joueur
   * @param context le contexte
   */
  logOutsideNextCoord(currCoord: Coordinate, nextCoord: Coordinate, playerName: string, context: string) {
    this.logger.warn(`${playerName} can't go out of the board !`, context);
    this.logInvalidNextMovement(currCoord, nextCoord, playerName, context);
  }

  /**
   * Log un avertissement sur un mouvement rencontrant une montagne
   * @param currCoord les coordonnées courantes du joueur
   * @param nextCoord les coordonnées suivantes du joueur
   * @param playerName le nom du joueur
   * @param context le contexte
   */
  logMountainNextCoord(currCoord: Coordinate, nextCoord: Coordinate, playerName: string, context: string) {
    this.logger.warn(`${playerName} can't climb the mountain !`, context);
    this.logInvalidNextMovement(currCoord, nextCoord, playerName, context);
  }

  /**
   * Log un avertissement sur un mouvement recontrant un autre joueur
   * @param currCoord les coordonnées courante du joueur
   * @param nextCoord les coordonnées suivante du joueur
   * @param playerName le nom du joueur
   * @param context le contexte
   */
  logAlreadyAdventurerNextCoord(currCoord: Coordinate, nextCoord: Coordinate, playerName: string, context: string) {
    this.logger.warn(`${playerName} can't join another player on the same cell !`, context);
    this.logInvalidNextMovement(currCoord, nextCoord, playerName, context);
  }

  /**
   * Log un avertissement sur un mouvement d'un joueur
   * @param param0 les coordonnées courante du joueur
   * @param param1 les coordonnées suivante du joueur
   * @param playerName le nom du joueur
   * @param context le contexte
   */
  private logInvalidNextMovement({ coordX: currX, coordY: currY }: Coordinate, { coordX: nextX, coordY: nextY }: Coordinate, playerName: string, context: string) {
    this.logger.warn(`Invalid next movement with coordinate X:${nextX} ; Y:${nextY}`, context);
    this.logger.warn(`${playerName} stays with coordinate X:${currX} ; Y:${currY}`, context);
  }
}
