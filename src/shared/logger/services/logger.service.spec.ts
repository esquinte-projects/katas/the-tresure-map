import { ResourceType } from '@models/enums/business';
import { Test } from '@nestjs/testing';
import { LoggerService } from './logger.service';

describe('LoggerService', () => {
  let service: LoggerService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [LoggerService],
    }).compile();

    service = moduleRef.get<LoggerService>(LoggerService);
  });

  describe('Error', () => {
    it('logError', () => {
      const mockLoggerError = jest.spyOn((service as any).logger, 'error').mockImplementation();

      service.logError("un message d'erreur", 'LoggerServiceSpec');

      expect(mockLoggerError).toHaveBeenCalledTimes(2);
      expect(mockLoggerError).toHaveBeenNthCalledWith(1, 'Error encoutered!', 'LoggerServiceSpec');
      expect(mockLoggerError).toHaveBeenNthCalledWith(2, "un message d'erreur", 'LoggerServiceSpec');
    });
  });

  describe('Coordinate', () => {
    it('logInvalidCoordinateX', () => {
      const mockLoggerWarn = jest.spyOn((service as any).logger, 'warn').mockImplementation();

      service.logInvalidCoordinateX({ coordX: '2', coordY: '1' }, { width: '2', height: '2' }, ResourceType.Enum.ADVENTURER, 'LoggerServiceSpec');

      expect(mockLoggerWarn).toHaveBeenCalledTimes(3);
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(1, 'Invalid coordinate X', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(2, 'The value must be between 0 and 1', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(3, 'Skipping this adventurer with coordinate X:2 ; Y:1', 'LoggerServiceSpec');
    });

    it('logInvalidCoordinateY', () => {
      const mockLoggerWarn = jest.spyOn((service as any).logger, 'warn').mockImplementation();

      service.logInvalidCoordinateY({ coordX: '1', coordY: '2' }, { width: '2', height: '2' }, ResourceType.Enum.ADVENTURER, 'LoggerServiceSpec');

      expect(mockLoggerWarn).toHaveBeenCalledTimes(3);
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(1, 'Invalid coordinate Y', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(2, 'The value must be between 0 and 1', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(3, 'Skipping this adventurer with coordinate X:1 ; Y:2', 'LoggerServiceSpec');
    });
  });

  describe('Dimension', () => {
    it('logInvalidWidth', () => {
      const mockLoggerWarn = jest.spyOn((service as any).logger, 'warn').mockImplementation();

      service.logInvalidWidth({ width: '0', height: '2' }, ResourceType.Enum.MAP, 'LoggerServiceSpec');

      expect(mockLoggerWarn).toHaveBeenCalledTimes(3);
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(1, 'Invalid width', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(2, 'The value must be at least 1', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(3, 'Skipping this map with width:0 and height:2', 'LoggerServiceSpec');
    });

    it('logInvalidHeight', () => {
      const mockLoggerWarn = jest.spyOn((service as any).logger, 'warn').mockImplementation();

      service.logInvalidHeight({ width: '2', height: '0' }, ResourceType.Enum.MAP, 'LoggerServiceSpec');

      expect(mockLoggerWarn).toHaveBeenCalledTimes(3);
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(1, 'Invalid height', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(2, 'The value must be at least 1', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(3, 'Skipping this map with width:2 and height:0', 'LoggerServiceSpec');
    });
  });

  describe('Map', () => {
    it('logDuplicateMap', () => {
      const mockLoggerWarn = jest.spyOn((service as any).logger, 'warn').mockImplementation();

      service.logDuplicateMap({ width: '2', height: '2' }, ResourceType.Enum.MAP, 'LoggerServiceSpec');

      expect(mockLoggerWarn).toHaveBeenCalledTimes(2);
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(1, 'A map has already been processed', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(2, 'Skipping this one with width:2 and height:2', 'LoggerServiceSpec');
    });

    it('logNoMapProcessed', () => {
      const mockLoggerError = jest.spyOn((service as any).logger, 'error').mockImplementation();

      service.logNoMapProcessed('LoggerServiceSpec');

      expect(mockLoggerError).toHaveBeenCalledTimes(3);
      expect(mockLoggerError).toHaveBeenNthCalledWith(1, 'There is no map in the input file', 'LoggerServiceSpec');
      expect(mockLoggerError).toHaveBeenNthCalledWith(2, 'Please add one as the first line of the file', 'LoggerServiceSpec');
      expect(mockLoggerError).toHaveBeenNthCalledWith(3, 'Syntax : C - {width} - {height}', 'LoggerServiceSpec');
    });
  });

  describe('Tresure', () => {
    it('logInvalidNumberOfTresure', () => {
      const mockLoggerWarn = jest.spyOn((service as any).logger, 'warn').mockImplementation();

      service.logInvalidNumberOfTresure({ coordX: '2', coordY: '2' }, ResourceType.Enum.ADVENTURER, 'LoggerServiceSpec');

      expect(mockLoggerWarn).toHaveBeenCalledTimes(3);
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(1, 'Invalid number of tresures', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(2, 'The value must be at least 1', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(3, 'Skipping this adventurer with coordinate X:2 ; Y:2', 'LoggerServiceSpec');
    });
  });

  describe('Adventurer', () => {
    it('logInvalidDirection', () => {
      const mockLoggerWarn = jest.spyOn((service as any).logger, 'warn').mockImplementation();

      service.logInvalidDirection({ coordX: '2', coordY: '2' }, ResourceType.Enum.ADVENTURER, 'LoggerServiceSpec');

      expect(mockLoggerWarn).toHaveBeenCalledTimes(3);
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(1, 'Invalid direction', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(2, 'The value must be any of N,S,E,O', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(3, 'Skipping this adventurer with coordinate X:2 ; Y:2', 'LoggerServiceSpec');
    });

    it('logInvalidMovements', () => {
      const mockLoggerWarn = jest.spyOn((service as any).logger, 'warn').mockImplementation();

      service.logInvalidMovements({ coordX: '2', coordY: '2' }, ResourceType.Enum.ADVENTURER, 'LoggerServiceSpec');

      expect(mockLoggerWarn).toHaveBeenCalledTimes(3);
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(1, 'Invalid movements', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(2, 'The values must be any of A,G,D', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(3, 'Skipping this adventurer with coordinate X:2 ; Y:2', 'LoggerServiceSpec');
    });
  });

  describe('Play', () => {
    it('logOutsideNextCoord', () => {
      const mockLoggerWarn = jest.spyOn((service as any).logger, 'warn').mockImplementation();

      service.logOutsideNextCoord({ coordX: '2', coordY: '2' }, { coordX: '3', coordY: '2' }, 'Lara', 'LoggerServiceSpec');

      expect(mockLoggerWarn).toHaveBeenCalledTimes(3);
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(1, "Lara can't go out of the board !", 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(2, 'Invalid next movement with coordinate X:3 ; Y:2', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(3, 'Lara stays with coordinate X:2 ; Y:2', 'LoggerServiceSpec');
    });

    it('logMountainNextCoord', () => {
      const mockLoggerWarn = jest.spyOn((service as any).logger, 'warn').mockImplementation();

      service.logMountainNextCoord({ coordX: '2', coordY: '2' }, { coordX: '3', coordY: '2' }, 'Lara', 'LoggerServiceSpec');

      expect(mockLoggerWarn).toHaveBeenCalledTimes(3);
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(1, "Lara can't climb the mountain !", 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(2, 'Invalid next movement with coordinate X:3 ; Y:2', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(3, 'Lara stays with coordinate X:2 ; Y:2', 'LoggerServiceSpec');
    });

    it('logAlreadyAdventurerNextCoord', () => {
      const mockLoggerWarn = jest.spyOn((service as any).logger, 'warn').mockImplementation();

      service.logAlreadyAdventurerNextCoord({ coordX: '2', coordY: '2' }, { coordX: '3', coordY: '2' }, 'Lara', 'LoggerServiceSpec');

      expect(mockLoggerWarn).toHaveBeenCalledTimes(3);
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(1, "Lara can't join another player on the same cell !", 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(2, 'Invalid next movement with coordinate X:3 ; Y:2', 'LoggerServiceSpec');
      expect(mockLoggerWarn).toHaveBeenNthCalledWith(3, 'Lara stays with coordinate X:2 ; Y:2', 'LoggerServiceSpec');
    });
  });
});
