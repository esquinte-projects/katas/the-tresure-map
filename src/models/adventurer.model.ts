import { Direction, Movement, ResourceType } from './enums/business';
import { Resource } from './interfaces';

/**
 * Modèle représentant un aventurier
 */
export class Adventurer implements Resource {
  x: number;
  y: number;
  type: ResourceType.Enum;
  nbTresures = 0;

  constructor(public name: string, x: number, y: number, public direction: Direction.Enum, public movements: Movement.Enum[]) {
    this.x = x;
    this.y = y;
    this.type = ResourceType.Enum.ADVENTURER;
  }
}
