import { Adventurer } from './adventurer.model';
import { Resource } from './interfaces/resource.interface';
import { Map } from './map.model';

/**
 * Modèle représentant un monde
 */
export class World {
  map: Map;
  mountains: Resource[] = [];
  tresures: Resource[] = [];
  adventurers: Adventurer[] = [];
}
