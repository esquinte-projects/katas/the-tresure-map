import { Cell } from './cell.model';

/**
 * Modèle représentant une carte
 */
export class Map {
  board: Cell[][];

  constructor(public width: number, public height: number) {}
}
