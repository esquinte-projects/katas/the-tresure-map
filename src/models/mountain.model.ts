import { ResourceType } from './enums/business';
import { Resource } from './interfaces/resource.interface';

/**
 * Modèle représentant une montagne
 */
export class Mountain implements Resource {
  x: number;
  y: number;
  type: ResourceType.Enum;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
    this.type = ResourceType.Enum.MOUNTAIN;
  }
}
