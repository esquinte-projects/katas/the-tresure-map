import { Adventurer } from './adventurer.model';
import { Resource } from './interfaces';

/**
 * Modèle représentant une cellule
 */
export class Cell {
  resource: Resource;
  adventurer: Adventurer;
}
