import { ResourceType } from '../enums/business';

/**
 * Interface représentant une ressource
 */
export interface Resource {
  x: number;
  y: number;
  type: ResourceType.Enum;
}
