/**
 * Interface représentant des dimensions
 */
export interface Dimension {
  width: string;
  height: string;
}
