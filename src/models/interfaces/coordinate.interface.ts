/**
 * Interface représentant des coordonnées
 */
export interface Coordinate {
  coordX: string | number;
  coordY: string | number;
}
