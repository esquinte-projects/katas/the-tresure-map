import { ResourceType } from './enums/business';
import { Resource } from './interfaces';

/**
 * Modèle représentant un trésor
 */
export class Tresure implements Resource {
  x: number;
  y: number;
  type: ResourceType.Enum;

  constructor(x: number, y: number, public nb: number) {
    this.x = x;
    this.y = y;
    this.type = ResourceType.Enum.TRESURE;
  }
}
