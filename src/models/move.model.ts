import { Direction, Movement } from './enums/business';

/**
 * Modèle représentant un mouvement
 */
export class Move {
  nextX: number;
  nextY: number;

  nextDirection: Direction.Enum;

  constructor(public currentX: number, public currentY: number, public direction: Direction.Enum, public state: Movement.Enum) {
    this.nextX = currentX;
    this.nextY = currentY;
    this.nextDirection = direction;
  }
}
