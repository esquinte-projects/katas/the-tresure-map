/**
 * Enum représentant le type d'évènement d'un read stream
 */
export enum ReadStreamEvent {
  OPEN = 'open',
  ERROR = 'error',
}
