/**
 * Enum représentant le type de coordonnée
 */
export enum CoordinateEnum {
  X = 'X',
  Y = 'Y',
}
