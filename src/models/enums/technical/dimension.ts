/**
 * Enum représentant le type de dimension
 */
export enum DimensionEnum {
  WIDTH = 'width',
  HEIGHT = 'height',
}
