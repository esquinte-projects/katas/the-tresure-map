/**
 * Enum représentant les directions des aventuriers
 */
export enum Enum {
  NORTH = 'N',
  SOUTH = 'S',
  EAST = 'E',
  WEST = 'O',
}

/**
 * Récupère les valeurs de l'enum
 * @returns la liste des valeurs
 */
export const getValues = (): string[] => Object.values(Enum);
