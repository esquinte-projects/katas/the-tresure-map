import { Direction } from '..';

describe('Direction', () => {
  describe('getValues', () => {
    it('should return values', () => {
      const values = Direction.getValues();
      expect(values).toHaveLength(4);
      expect(values).toEqual(['N', 'S', 'E', 'O']);
    });
  });
});
