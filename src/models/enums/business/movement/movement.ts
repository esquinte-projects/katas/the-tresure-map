/**
 * Enum représentant les mouvements des aventuriers
 */
export enum Enum {
  FORWARD = 'A',
  LEFT = 'G',
  RIGHT = 'D',
}

/**
 * Récupère les valeurs de l'enum
 * @returns la liste des valeurs
 */
export const getValues = (): string[] => Object.values(Enum);
