import { Movement } from '..';

describe('Movement', () => {
  describe('getValues', () => {
    it('should return values', () => {
      const values = Movement.getValues();
      expect(values).toHaveLength(3);
      expect(values).toEqual(['A', 'G', 'D']);
    });
  });
});
