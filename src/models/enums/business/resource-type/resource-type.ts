/**
 * Enum représentant le type de ressource
 */
export enum Enum {
  MAP = 'C',
  MOUNTAIN = 'M',
  TRESURE = 'T',
  ADVENTURER = 'A',
}

/**
 * Récupère la clé de l'enum en fonction de sa valeur
 * @param value la valeur de l'enum
 * @returns la clé correspondante
 */
export const getResourceTypeKeyFromValue = (value: string): string => {
  return Object.entries(Enum)
    .filter((entry) => entry[1] === value)
    .map((entry) => entry[0])[0]
    .toLowerCase();
};

/**
 * Récupère les valeurs de l'enum
 * @returns la liste des valeurs
 */
export const getValues = (): string[] => Object.values(Enum);
