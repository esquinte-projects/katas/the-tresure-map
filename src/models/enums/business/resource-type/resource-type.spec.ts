import { ResourceType } from '..';

describe('Movement', () => {
  describe('getResourceTypeKeyFromValue', () => {
    it('should return key from value', () => {
      let key = ResourceType.getResourceTypeKeyFromValue('C');
      expect(key).toBe('map');

      key = ResourceType.getResourceTypeKeyFromValue('M');
      expect(key).toBe('mountain');

      key = ResourceType.getResourceTypeKeyFromValue('T');
      expect(key).toBe('tresure');

      key = ResourceType.getResourceTypeKeyFromValue('A');
      expect(key).toBe('adventurer');

      expect(() => ResourceType.getResourceTypeKeyFromValue('Z')).toThrowError();
    });
  });

  describe('getValues', () => {
    it('should return values', () => {
      const values = ResourceType.getValues();
      expect(values).toHaveLength(4);
      expect(values).toEqual(['C', 'M', 'T', 'A']);
    });
  });
});
