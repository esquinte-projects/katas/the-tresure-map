# Exercice pratique - La carte aux trésors

### Guidez les aventuriers en quête de trésors !

Exercice proposé par Carbon IT

## Lancer le projet

- Installation des dépendances

```
$>npm install
```

- Démarrer l'application

```
$>npm start
```

## Lancer les tests

- Tests

```
$>npm run test
```

- Coverage

```
$>npm run test:cov
```

> ### <ins>Notes:</ins>
>
> Le fichier d'entrée est situé dans "./input/init.txt"
>
> Les fichiers de sortie sont situés dans "./output/result.txt" et "./output/graph.txt"
>
> La configuration se trouve dans le fichier "./config.env", de même pour les tests le fichier se trouve dans "test/config-test.env"
